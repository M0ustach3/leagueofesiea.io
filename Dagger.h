#ifndef DAGGER_H
#define DAGGER_H

#include <string>
#include <vector>

#include "Melee.h"

/**
 * Class representing a Dagger
 */
class Dagger : public Melee {
public:
    /**
     * Constructor of a Dagger
     * @param mName the name
     * @param mDamage the damage
     * @param mCritical the critical probability
     * @param mDurability the durability
     * @param mIsEnchanted if the dagger in enchanted or not
     */
    Dagger(const std::string &mName, float mDamage, float mCritical, int mDurability, bool mIsEnchanted);

    /**
     * Destructor of a Dagger
     */
    virtual ~Dagger();
};

#endif
