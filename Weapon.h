#ifndef WEAPON_H
#define WEAPON_H

#include <string>
#include <map>

/**
 * Class representing a weapon
 */
class Weapon {
protected:
    /**
     * The name of the weapon
     */
    std::string m_name;
    /**
     * The damage of the owner
     */
    float m_damage;
    /**
     * The critical probability
     */
    float m_critical;
    /**
     * The bonuses of the weapon
     */
    std::map<std::string, float>
            m_bonus;
public:
    /**
     * Constructor of a weapon
     * @param mName the name
     * @param mDamage the damage
     * @param mCritical the critical probability
     */
    Weapon(const std::string &mName, float mDamage, float mCritical);

    /**
     * Destructor
     */
    virtual ~Weapon();

    /**
     * Getter for the name
     * @return the name of the weapon
     */
    const std::string &getMName() const;

    /**
     * Getter for the damages
     * @return the damages of the weapon
     */
    float getMDamage() const;

    /**
     * Setter for the damage of the weapon
     * @param mDamage the new damage value
     */
    void setMDamage(float mDamage);

    /**
     * Getter for the critical value
     * @return the critical value
     */
    float getMCritical() const;

    /**
     * Getter for the bonuses of the weapons
     * @return a map of bonuses
     */
    const std::map<std::string, float> &getMBonus() const;

    /**
     * Setter for the bonuses of the weapon
     * @param mBonus the new bonuses
     */
    void setMBonus(const std::map<std::string, float> &mBonus);

};

#endif
