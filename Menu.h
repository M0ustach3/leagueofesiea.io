#ifndef LEAGUEOFESIEA_IO_MENU_H
#define LEAGUEOFESIEA_IO_MENU_H


#include <vector>
#include <string>
#include "Character.h"
#include "CharacterParser.h"
#include "WeaponParser.h"

/**
 * Class describing the menu
 */
class Menu {
private:
    /**
     * The cursor position for the current menu
     */
    int m_cursorPosition;
    /**
     * The choices printed on screen
     */
    std::vector<std::string> m_choices;

public:
    /**
     * Constructor for the menu
     */
    Menu();

    /**
     * Method used to print the character selection menu
     * @param p_currentPlayer the player who has to select his characters
     * @param g the current game
     */
    void printCharacterSelectionMenuRound(Player *p_currentPlayer, Game *g);

    /**
     * Method used to print the ability selection menu
     * @param p_characterSelected the character selected in the previous menu
     * @param g the current game
     */
    void printAbilitySelectionMenu(Character *p_characterSelected, Game *g);

    /**
     * Method used to print the target selection for the ability
     * @param p_characterSelected the selected character
     * @param g the current game
     */
    void printTargetSelectionForAbility(Character *p_characterSelected, Game *g);

    /**
     * Method used to print the action selection
     * @param p_characterSelected the selected character
     * @param g the current game
     */
    void printActionSelectionMenu(Character *p_characterSelected, Game *g);

    /**
     * Method used to print the target selection for the punch
     * @param p_characterSelected the selected character
     * @param g the current game
     */
    void printTargetSelectionForPunch(Character *p_characterSelected, Game *g);

    /**
     * Method used to print the target selection for the attack with a weapon
     * @param p_characterSelected the selected character
     * @param g the current game
     */
    void printTargetSelectionForAttackWithWeapon(Character *p_charactedSelected, Game *g);

    /**
     * Method used to print infos on a character
     * @param p_c the character to print
     */
    void printCharacterInfo(Character *p_c);

    /**
     * Method used to print infos on a weapon
     * @param p_weapon the weapon to print
     */
    void printWeaponInfo(Weapon *p_weapon);

    /**
     * Destructor for the menu
     */
    virtual ~Menu();

    /**
     * Method used to print the character selection menu
     * @param p_currentPlayer the current player who has to select
     * @param p_availableCharacters the available characters for the player
     * @param p_availableWeapons the available weapons for the player
     */
    void chooseCharacter(Player *p_currentPlayer, std::vector<CharacterParser> *p_availableCharacters,
                         std::vector<WeaponParser> *p_availableWeapons);

    /**
     * Method used to print the weapon selection menu
     * @param p_currentPlayer the current player
     * @param p_availableWeapons the available weapons
     */
    void chooseWeapons(Player *p_currentPlayer, std::vector<Weapon *> *p_availableWeapons);

    /**
     * Method used to choose the player's names
     * @param p_game the current game
     */
    void choosePlayers(Game *p_game);

};

#endif //LEAGUEOFESIEA_IO_MENU_H
