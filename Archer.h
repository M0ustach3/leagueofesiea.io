#include "Character.h"
#include "Dagger.h"
#include "Bow.h"
#include <string>
#include <vector>

#ifndef ARCHER_H
#define ARCHER_H

/**
 *  Class representing an Archer
 *  @see Character
 */
class Archer : public Character {
private:
    /**
     * Field representing if the archer is aiming or not
     */
    bool m_isAiming;
    /**
     * Field representing the number of usages of the antidote ability
     */
    unsigned char m_usages;

public:
    /**
     * Constructor of an archer
     * @param mName the name
     * @param mLife the life
     * @param mDodge the dodge chance
     * @param mSpeed the speed
     * @param mAttack the attack
     * @param mDefense the defense
     * @param mAgility the agility
     * @param mIntelligence the intelligence
     */
    Archer(const std::string &mName, int mLife, float mDodge, int mSpeed, int mAttack, int mDefense, int mAgility,
           int mIntelligence);

    /**
     * Method used to aim on
     */
    void aimOn();

    /**
     * Method used to aim off
     */
    void aimOff();

    /**
     * Method used to get the usages of the antidote ability
     * @return the number of remaining usages
     */
    unsigned char getMUsages() const;

    /**
     * Method used to give an antidote to the character provided in parameter
     * @param p_target the target to heal from poison
     */
    void antidote(Character *p_target);

    /**
     * Destructor of archer
     */
    virtual ~Archer();

    /**
     * See if the archer is aiming
     * @return true if the archer is aiming, false otherwise
     */
    bool isMIsAiming() const;

    /**
     * Method used to get a list of the abilities of the archer
     * @return a vector of string, containing the abilities of the archer
     */
    std::vector<std::string> getAbilities() override;

    /**
     * Method used to get the weapon of the archer
     * @return a pointer to the weapon of the archer
     */
    Weapon *getMWeapon() const override;

    /**
     * Method used to set the weapon of the archer
     * @param mWeapon the new weapon
     */
    void setMWeapon(Weapon *mWeapon) override;

};

#endif
