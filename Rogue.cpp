#include "Rogue.h"
#include "exceptions/GameException.hpp"


void Rogue::vanish() {
    if (this->m_cooldownVanish <= 0) {
        if (!this->m_isInvisible) {
            //The rogue is invisible until the end of the cooldown
            //not good
            this->setMIsInvisible(true);
            std::cout << this->getMName() << " disappears in the shadows..." << std::endl;
            this->m_cooldownVanish = 4;
        } else {
            throw VanishException(this->m_name + " is already invisible");
        }
    } else {
        throw VanishException(this->m_name + " should wait " + std::to_string(this->m_cooldownVanish) +
                              " more rounds to try to vanish again");
    }
}

Rogue::~Rogue() {
    //Do not destruct weapon here, do it in main

}

float Rogue::getMPoison() const {
    return m_poison;
}

bool Rogue::isMIsInvisible() const {
    return m_isInvisible;
}

void Rogue::setMIsInvisible(bool mIsInvisible) {
    m_isInvisible = mIsInvisible;
}

Rogue::Rogue(const std::string &mName, int mLife, float mDodge, int mSpeed, int mAttack, int mDefense, int mAgility,
             int mIntelligence, float mPoison) : Character(mName, mLife, mDodge,
                                                           mSpeed, mAttack,
                                                           mDefense, mAgility,
                                                           mIntelligence),
                                                 m_poison(mPoison),
                                                 m_isInvisible(false), m_cooldownVanish(0) {

}

Weapon *Rogue::getMWeapon() const {
    return Character::getMWeapon();
}

void Rogue::setMWeapon(Weapon *mWeapon) {
    if (Game::getTypeOfWeapon(mWeapon) == "Bow" || Game::getTypeOfWeapon(mWeapon) == "Dagger") {
        Character::setMWeapon(mWeapon);
    } else {
        std::cerr << "Incompatible weapon" << std::endl;
    }
}

unsigned short Rogue::getMCooldownVanish() const {
    return m_cooldownVanish;
}

void Rogue::decreaseVanishCooldown() {
    if (this->m_cooldownVanish > 0) {
        this->m_cooldownVanish--;
    }
}

void Rogue::decrementAllCooldowns() {
    this->decreaseVanishCooldown();
}

std::vector<std::string> Rogue::getAbilities() {
    std::vector<std::string> abilities;

    if (this->getMCooldownVanish() > 0)
        abilities.push_back("Vanish (Not available, " + std::to_string(this->getMCooldownVanish()) + " rounds left)");
    else
        abilities.push_back("Vanish");

    return abilities;
}
