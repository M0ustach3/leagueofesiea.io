#include <iostream>

struct ParserException : public std::exception {
public:
    explicit ParserException(std::string filename) : exception() {
        this->filename = filename;
    };

    virtual ~ParserException() = default;

    virtual const char *
    what() noexcept {
        return this->filename.c_str();
    }

private:
    std::string filename;
};
