#include <iostream>

struct GameException : public std::exception {
public:
    GameException() = default;

    virtual ~GameException() = default;

    virtual const char *what() noexcept { return ""; }
};

struct ManaException : public GameException {
public:
    explicit ManaException(std::string reason) : GameException() {
        this->reason = reason;
    };

    virtual ~ManaException() = default;

    virtual const char *
    what() noexcept {
        return this->reason.c_str();
    }

private:
    std::string reason;
};

struct ArrowsException : public GameException {
    ArrowsException() = default;

    virtual ~ArrowsException() = default;

    const char *what() noexcept { return "Not enough arrows"; }
};

struct VanishException : public GameException {
public:
    explicit VanishException(std::string reason) : GameException() {
        this->reason = reason;
    };

    virtual ~VanishException() = default;

    virtual const char *
    what() noexcept {
        return this->reason.c_str();
    }

private:
    std::string reason;
};

struct AntidoteException : public GameException {
public:
    explicit AntidoteException(std::string reason) : GameException() {
        this->reason = reason;
    };

    virtual ~AntidoteException() = default;

    virtual const char *
    what() noexcept {
        return this->reason.c_str();
    }

private:
    std::string reason;
};

struct HealException : public GameException {
public:
    explicit HealException(std::string reason) : GameException() {
        this->reason = reason;
    };

    virtual ~HealException() = default;

    virtual const char *
    what() noexcept {
        return this->reason.c_str();
    }

private:
    std::string reason;
};

struct AimException : public GameException {
public:
    explicit AimException(std::string reason) : GameException() {
        this->reason = reason;
    };

    virtual ~AimException() = default;

    virtual const char *
    what() noexcept {
        return this->reason.c_str();
    }

private:
    std::string reason;
};