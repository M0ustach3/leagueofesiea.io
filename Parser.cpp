#include <iostream>
#include <sstream>
#include "Parser.h"
#include "Sorcerer.h"
#include "Warrior.h"
#include "Archer.h"
#include "Rogue.h"
#include "exceptions/ParserException.hpp"

/// Function used to generate an array of characters to be used in the main menu
/// \param p_inputFile the path of the file to be parsed. NOTE THAT THIS FILE MUST BE
/// IN ONE FOLDER ABOVE THE EXECUTABLE OTHERWISE IT WON'T WORK
/// \return an array of parsed characters
std::vector<CharacterParser> Parser::parseCharactersFromFile(std::string p_inputFile) {
    //Input the file
    std::ifstream file(p_inputFile, std::ios::in);
    //A var to hold the result
    std::vector<CharacterParser> RESULT;
    // If the file was successfully opened
    if (file) {
        std::string currentLine, test;
        //A var for the lines parsed from the file
        std::vector<std::string> contents;
        //Get the lines from the file and put the lines in the vector
        while (getline(file, currentLine)) {
            //Truncate the line if it contains a \r
            if (currentLine[currentLine.size() - 1] == '\r') {
                currentLine.resize(currentLine.size() - 1);
            }
            contents.push_back(currentLine);
        }
        //The iterator for the contents
        std::vector<std::string>::iterator iterator;
        //A var used to hold a couple of values of one line
        std::vector<std::string> couple;
        //A value separated by a \t
        std::string substr;

        //Iterate over the lines
        for (iterator = contents.begin(); iterator != contents.end(); iterator++) {
            //If this is the start of a character description
            if (*iterator == "Character") {
                //Go to the next line
                iterator++;
                //Loop until finding the end of the description
                while (*iterator != "EndCharacter") {
                    //Create an input string stream to separate each value from one line
                    std::istringstream iss(*iterator);
                    //Separate the values with a \t
                    while (getline(iss, substr, '\t')) {
                        //This is to avoid a problem when using an inconsistent number of \t to separate the values
                        if (substr.empty()) {
                            continue;
                            //Push back a parsed value in a couple array
                        } else {
                            couple.push_back(substr);
                        }
                    }
                    //Loop in the next line
                    iterator++;
                }
                //Create a temporary character
                CharacterParser temporary;
                //For each value of the character
                for (int i = 0; i < couple.size(); ++i) {
                    //If the first value of the line is the class
                    if (couple[i] == "Classe") {
                        //Set the class of the temporary character to the next value
                        temporary.setMClasse(couple[i + 1]);
                    } else if (couple[i] == "Nom") {
                        temporary.setMNom(couple[i + 1]);
                    } else if (couple[i] == "Vitesse") {
                        temporary.setMVitesse(couple[i + 1]);
                    } else if (couple[i] == "Attaque") {
                        temporary.setMAttaque(couple[i + 1]);
                    } else if (couple[i] == "Intelligence") {
                        temporary.setMIntelligence(couple[i + 1]);
                    } else if (couple[i] == "HP") {
                        temporary.setMHp(couple[i + 1]);
                    } else if (couple[i] == "Defense") {
                        temporary.setMDefense(couple[i + 1]);
                    } else if (couple[i] == "Dodge") {
                        temporary.setMDodge(couple[i + 1]);
                    } else if (couple[i] == "Agilite") {
                        temporary.setMAgilite(couple[i + 1]);
                    } else if (couple[i] == "Special") {
                        temporary.setMSpecial(couple[i + 1]);
                    }
                }
                //Add the temporary character in the result array
                RESULT.push_back(temporary);
                //Clear the values used in the first character
                couple.clear();
            }
        }
        return RESULT;
    } else {
        throw ParserException(p_inputFile);
    }
}

/// Function used to generate an array of weapons to be used in the main menu
/// \param p_inputFile the path of the file to be parsed. NOTE THAT THIS FILE MUST BE
/// IN ONE FOLDER ABOVE THE EXECUTABLE OTHERWISE IT WON'T WORK
/// \return an array of parsed weapons
std::vector<WeaponParser> Parser::parseWeaponsFromFile(std::string p_inputFile) {
    //Input the file
    std::ifstream file(p_inputFile, std::ios::in);
    //A var to hold the result
    std::vector<WeaponParser> RESULT;
    // If the file was successfully opened
    if (file) {
        std::string currentLine, test;
        //A var for the lines parsed from the file
        std::vector<std::string> contents;
        //Get the lines from the file and put the lines in the vector
        while (getline(file, currentLine)) {
            if (currentLine.empty()) continue;
            //Truncate the line if it contains a \r
            if (currentLine[currentLine.size() - 1] == '\r') {
                currentLine.resize(currentLine.size() - 1);
            }
            contents.push_back(currentLine);
        }
        //The iterator for the contents
        std::vector<std::string>::iterator iterator;
        //A var used to hold a couple of values of one line
        std::vector<std::string> couple;
        //A value separated by a \t
        std::string substr;

        //Iterate over the lines
        for (iterator = contents.begin(); iterator != contents.end(); iterator++) {
            //If this is the start of a weapon description
            if (*iterator == "Weapon") {
                //Go to the next line
                iterator++;
                //Loop until finding the end of the description
                while (*iterator != "EndWeapon") {
                    //Create an input string stream to separate each value from one line
                    std::istringstream iss(*iterator);
                    //Separate the values with a \t
                    while (getline(iss, substr, '\t')) {
                        //This is to avoid a problem when using an inconsistent number of \t to separate the values
                        if (substr.empty()) {
                            continue;
                            //Push back a parsed value in a couple array
                        } else {
                            couple.push_back(substr);
                        }
                    }
                    //Loop in the next line
                    iterator++;
                }
                //Create a temporary weapon
                WeaponParser temporary;
                //For each value of the weapon
                for (int i = 0; i < couple.size(); ++i) {
                    //If the first value of the line is the class
                    if (couple[i] == "Type") {
                        //Set the class of the temporary weapon to the next value
                        temporary.setMType(couple[i + 1]);
                    } else if (couple[i] == "Nom") {
                        temporary.setMNom(couple[i + 1]);
                    } else if (couple[i] == "Intelligence") {
                        temporary.setMIntelligence(couple[i + 1]);
                    } else if (couple[i] == "HP") {
                        temporary.setMHp(couple[i + 1]);
                    } else if (couple[i] == "Agilite") {
                        temporary.setMAgilite(couple[i + 1]);
                    } else if (couple[i] == "Cout") {
                        temporary.setMCout(couple[i + 1]);
                    } else if (couple[i] == "Critique") {
                        temporary.setMCritique(couple[i + 1]);
                    } else if (couple[i] == "Degat") {
                        temporary.setMDegat(couple[i + 1]);
                    } else if (couple[i] == "Attack") {
                        temporary.setMAttack(couple[i + 1]);
                    } else if (couple[i] == "Durabilite") {
                        temporary.setMDurabilite(couple[i + 1]);
                    } else if (couple[i] == "Fleche") {
                        temporary.setMFleche(couple[i + 1]);
                    }
                }
                //Add the temporary weapon in the result array
                RESULT.push_back(temporary);
                //Clear the values used in the first weapon
                couple.clear();
            }
        }
        return RESULT;
    } else {
        throw ParserException(p_inputFile);
    }
}

Character *Parser::getCharacterFromCharacterParser(CharacterParser *p_characterParser) {
    const std::string &characterClass = p_characterParser->getMClasse();
    if (characterClass == "Mage") {
        return new Sorcerer(p_characterParser->getMNom(), std::stoi(p_characterParser->getMHp()),
                            std::stof(p_characterParser->getMDodge()),
                            std::stoi(p_characterParser->getMVitesse()),
                            std::stoi(p_characterParser->getMAttaque()),
                            std::stoi(p_characterParser->getMDefense()),
                            std::stoi(p_characterParser->getMAgilite()),
                            std::stoi(p_characterParser->getMIntelligence()));
    } else if (characterClass == "Guerrier") {
        return new Warrior(p_characterParser->getMNom(), std::stoi(p_characterParser->getMHp()),
                           std::stof(p_characterParser->getMDodge()),
                           std::stoi(p_characterParser->getMVitesse()),
                           std::stoi(p_characterParser->getMAttaque()),
                           std::stoi(p_characterParser->getMDefense()),
                           std::stoi(p_characterParser->getMAgilite()),
                           std::stoi(p_characterParser->getMIntelligence()),
                           std::stof(p_characterParser->getMSpecial()));
    } else if (characterClass == "Archer") {
        return new Archer(p_characterParser->getMNom(), std::stoi(p_characterParser->getMHp()),
                          std::stof(p_characterParser->getMDodge()),
                          std::stoi(p_characterParser->getMVitesse()),
                          std::stoi(p_characterParser->getMAttaque()),
                          std::stoi(p_characterParser->getMDefense()),
                          std::stoi(p_characterParser->getMAgilite()),
                          std::stoi(p_characterParser->getMIntelligence()));
    } else if (characterClass == "Voleur") {
        return new Rogue(p_characterParser->getMNom(), std::stoi(p_characterParser->getMHp()),
                         std::stof(p_characterParser->getMDodge()),
                         std::stoi(p_characterParser->getMVitesse()),
                         std::stoi(p_characterParser->getMAttaque()),
                         std::stoi(p_characterParser->getMDefense()),
                         std::stoi(p_characterParser->getMAgilite()),
                         std::stoi(p_characterParser->getMIntelligence()),
                         std::stof(p_characterParser->getMSpecial()));
    } else {
        return nullptr;
    }
}
