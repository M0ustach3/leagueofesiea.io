#include "CharacterParser.h"

CharacterParser::CharacterParser() {
    this->m_nom = "";
    this->m_agilite = "";
    this->m_attaque = "";
    this->m_classe = "";
    this->m_defense = "";
    this->m_dodge = "";
    this->m_hp = "";
    this->m_vitesse = "";
    this->m_special = "";
    this->m_intelligence = "";
};

void CharacterParser::setMClasse(const std::string &mClasse) {
    m_classe = mClasse;
}

void CharacterParser::setMNom(const std::string &mNom) {
    m_nom = mNom;
}

void CharacterParser::setMVitesse(const std::string &mVitesse) {
    m_vitesse = mVitesse;
}

void CharacterParser::setMAttaque(const std::string &mAttaque) {
    m_attaque = mAttaque;
}

void CharacterParser::setMIntelligence(const std::string &mIntelligence) {
    m_intelligence = mIntelligence;
}

void CharacterParser::setMHp(const std::string &mHp) {
    m_hp = mHp;
}

void CharacterParser::setMDefense(const std::string &mDefense) {
    m_defense = mDefense;
}

void CharacterParser::setMDodge(const std::string &mDodge) {
    m_dodge = mDodge;
}

void CharacterParser::setMAgilite(const std::string &mAgilite) {
    m_agilite = mAgilite;
}

void CharacterParser::setMSpecial(const std::string &mSpecial) {
    m_special = mSpecial;
}

std::ostream &operator<<(std::ostream &os, const CharacterParser &parser) {
    os << "m_classe: " << parser.m_classe << " m_nom: " << parser.m_nom << " m_vitesse: " << parser.m_vitesse
       << " m_attaque: " << parser.m_attaque << " m_intelligence: " << parser.m_intelligence << " m_hp: " << parser.m_hp
       << " m_defense: " << parser.m_defense << " m_dodge: " << parser.m_dodge << " m_agilite: " << parser.m_agilite
       << " m_special: " << parser.m_special;
    return os;
}

const std::string &CharacterParser::getMClasse() const {
    return m_classe;
}

const std::string &CharacterParser::getMNom() const {
    return m_nom;
}

const std::string &CharacterParser::getMVitesse() const {
    return m_vitesse;
}

const std::string &CharacterParser::getMAttaque() const {
    return m_attaque;
}

const std::string &CharacterParser::getMIntelligence() const {
    return m_intelligence;
}

const std::string &CharacterParser::getMHp() const {
    return m_hp;
}

const std::string &CharacterParser::getMDefense() const {
    return m_defense;
}

const std::string &CharacterParser::getMDodge() const {
    return m_dodge;
}

const std::string &CharacterParser::getMAgilite() const {
    return m_agilite;
}

const std::string &CharacterParser::getMSpecial() const {
    return m_special;
}
