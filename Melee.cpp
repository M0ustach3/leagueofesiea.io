#include "Melee.h"

Melee::Melee(const std::string &mName, float mDamage, float mCritical, int mDurability, bool mIsEnchanted) : Weapon(
        mName, mDamage, mCritical), m_durability(mDurability), m_isEnchanted(mIsEnchanted), BASE_DURABILITY(
        mDurability) {}

int Melee::getMDurability() const {
    return m_durability;
}

void Melee::setMDurability(int mDurability) {
    m_durability = mDurability;
}

bool Melee::isMIsEnchanted() const {
    return m_isEnchanted;
}

void Melee::setMIsEnchanted(bool mIsEnchanted) {
    m_isEnchanted = mIsEnchanted;
}

Melee::~Melee() {}

int Melee::getBaseDurability() const {
    return BASE_DURABILITY;
}
