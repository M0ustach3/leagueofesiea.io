#include <iostream>
#include "Menu.h"
#include "Parser.h"
#include "Archer.h"
#include "Rogue.h"
#include "Warrior.h"
#include "Sorcerer.h"
#include "exceptions/GameException.hpp"

namespace colors {
    std::string red = "\033[1;31m";
    std::string orange = "\033[1;33m";
    std::string green = "\033[1;32m";
    std::string clear = "\033[1;0m";
    std::string magenta = "\033[1;35m";
    std::string blue = "\033[1;34m";
}


/// Method that print the character to play with for this round
/// \param p_currentPlayer Player who is playing at the moment
/// \param g Current game
void Menu::printCharacterSelectionMenuRound(Player *p_currentPlayer, Game *g) {

    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;
    std::cout << colors::green << p_currentPlayer->getMName()
              << colors::clear << "'s turn !" << std::endl;
    std::cout << "Select the character you want to use (Z/S to navigate, D to confirm) :" << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;

    std::vector<Character *>::iterator itChar;
    std::vector<Character *> c = p_currentPlayer->getMCharacters();
    for (itChar = c.begin(); itChar != c.end(); ++itChar) {
        this->m_choices.push_back("  " + (*itChar)->getMName());
    }

    std::vector<std::string>::iterator itChoices;
    for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
        //Check if pointed character is dead
        while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
            this->m_cursorPosition++;
        }

        if (itChoices - this->m_choices.begin() == this->m_cursorPosition)
            itChoices->at(0) = '>';

        std::cout << *itChoices << std::endl;
    }

    bool choiceCompleted = false;
    char input;
    Character *characterSelected = nullptr;

    while (!choiceCompleted) {

        std::cin >> input;

        switch (input) {
            case 'z': //UP

                if (system("CLS")) system("clear");

                std::cout << "Select the character you want to use (Z/S to navigate, D to confirm) :" << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition > 0)
                    this->m_cursorPosition--;
                else
                    this->m_cursorPosition = this->m_choices.size() - 1;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == 0)
                        this->m_cursorPosition = this->m_choices.size() - 1;
                    else
                        this->m_cursorPosition--;
                }


                for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
                    if (itChoices - this->m_choices.begin() == this->m_cursorPosition)
                        itChoices->at(0) = '>';
                    else
                        itChoices->at(0) = ' ';

                    std::cout << *itChoices << std::endl;
                }
                break;
            case 's': //DOWN

                if (system("CLS")) system("clear");

                std::cout << "Select the character you want to use (Z/S to navigate, D to confirm) :" << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition < this->m_choices.size() - 1)
                    this->m_cursorPosition++;
                else
                    this->m_cursorPosition = 0;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == this->m_choices.size() - 1)
                        this->m_cursorPosition = 0;
                    else
                        this->m_cursorPosition++;
                }


                for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
                    if (itChoices - this->m_choices.begin() == this->m_cursorPosition)
                        itChoices->at(0) = '>';
                    else
                        itChoices->at(0) = ' ';

                    std::cout << *itChoices << std::endl;
                }
                break;
            case 'd':
                if (system("CLS")) system("clear");
                choiceCompleted = true;
                this->printActionSelectionMenu(p_currentPlayer->getMCharacters().at(this->m_cursorPosition), g);
                break;
            default:
                std::cout << "tu es le meilleur c'est pas grave" << std::endl;
        }
    }
}

/// Method that print the menu to choose 3 characters to play with
/// \param p_currentPlayer Player who is playing at the moment
/// \param p_availableCharacters Characters available in the game, parsed previously from a config file 'characters.jdc'
void Menu::chooseCharacter(Player *p_currentPlayer, std::vector<CharacterParser> *p_availableCharacters,
                           std::vector<WeaponParser> *p_availableWeapons) {

    if (system("CLS")) system("clear");

    std::cout << colors::green
              << p_currentPlayer->getMName()
              << colors::clear
              << ", select a character you want to put in your team (Z/S to navigate, I to print character informations,  D to confirm) :"
              << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;

    std::vector<CharacterParser>::iterator itChar;
    std::vector<CharacterParser> c = *p_availableCharacters;
    for (itChar = c.begin(); itChar != c.end(); ++itChar) {
        this->m_choices.push_back("  " + (*itChar).getMNom() + " (" +
                                  Game::getTypeOfCharacter(Parser::getCharacterFromCharacterParser(&(*itChar))) + ")");
    }

    std::vector<std::string>::iterator itChoices;
    for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
        if (itChoices - this->m_choices.begin() == 0) {
            itChoices->at(0) = '>';
            std::cout << colors::magenta << *itChoices << colors::clear << std::endl;
        } else {
            std::cout << *itChoices << std::endl;
        }
    }

    bool choiceCompleted = false;
    char input;
    Character *characterSelected = nullptr;
    std::vector<Character *> selectedCharacters;

    while (!choiceCompleted) {
        choiceCompleted = selectedCharacters.size() == 3;
        if (choiceCompleted) break;

        if (!selectedCharacters.empty()) {
            std::cout << colors::magenta
                      << "-------------------------------------------------------------------------------------------------"
                      << colors::clear
                      << std::endl;
            std::cout << colors::green
                      << p_currentPlayer->getMName()
                      << colors::clear
                      << " selected these players : " << std::endl;
            std::cout << colors::magenta
                      << "-------------------------------------------------------------------------------------------------"
                      << colors::clear
                      << std::endl;
            for (int i = 0; i < selectedCharacters.size(); ++i) {
                std::cout << selectedCharacters.at(i)->getMName() << std::endl;
            }
            if (!selectedCharacters.empty()) {
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
            }
        }

        std::cin >> input;

        switch (input) {
            case 'z': //UP

                if (system("CLS")) system("clear");

                std::cout << colors::green
                          << p_currentPlayer->getMName()
                          << colors::clear
                          << ", select a character you want to put in your team (Z/S to navigate, I to print character informations,  D to confirm) :"
                          << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition > 0)
                    this->m_cursorPosition--;
                else {
                    this->m_cursorPosition = this->m_choices.size() - 1;
                }
                for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
                    if (itChoices - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoices->at(0) = '>';
                        std::cout << colors::magenta << *itChoices << colors::clear << std::endl;
                    } else {
                        itChoices->at(0) = ' ';
                        std::cout << *itChoices << std::endl;
                    }
                }
                break;
            case 's': //DOWN

                if (system("CLS")) system("clear");

                std::cout << colors::green
                          << p_currentPlayer->getMName()
                          << colors::clear
                          << ", select a character you want to put in your team (Z/S to navigate, I to print character informations,  D to confirm) :"
                          << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition < this->m_choices.size() - 1)
                    this->m_cursorPosition++;
                else {
                    this->m_cursorPosition = 0;
                }
                for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
                    if (itChoices - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoices->at(0) = '>';
                        std::cout << colors::magenta << *itChoices << colors::clear << std::endl;
                    } else {
                        itChoices->at(0) = ' ';
                        std::cout << *itChoices << std::endl;
                    }
                }
                break;
            case 'd':
                if (system("CLS")) system("clear");
                std::cout << colors::green
                          << p_currentPlayer->getMName()
                          << colors::clear
                          << ", select a character you want to put in your team (Z/S to navigate, I to print character informations,  D to confirm) :"
                          << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                characterSelected = Parser::getCharacterFromCharacterParser(
                        &p_availableCharacters->at(m_cursorPosition));
                selectedCharacters.push_back(characterSelected);
                this->m_choices.erase(this->m_choices.begin() + this->m_cursorPosition);
                p_availableCharacters->erase(p_availableCharacters->begin() + this->m_cursorPosition);
                this->m_cursorPosition = 0;
                for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
                    if (itChoices - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoices->at(0) = '>';
                        std::cout << colors::magenta << *itChoices << colors::clear << std::endl;
                    } else {
                        itChoices->at(0) = ' ';
                        std::cout << *itChoices << std::endl;
                    }
                }
                break;
            case 'i':
                if (system("CLS")) system("clear");
                this->printCharacterInfo(
                        Parser::getCharacterFromCharacterParser(&p_availableCharacters->at(this->m_cursorPosition)));
                std::cout << colors::green
                          << p_currentPlayer->getMName()
                          << colors::clear
                          << ", select a character you want to put in your team (Z/S to navigate, I to print character informations,  D to confirm) :"
                          << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
                    if (itChoices - this->m_choices.begin() == this->m_cursorPosition)
                        itChoices->at(0) = '>';
                    else
                        itChoices->at(0) = ' ';

                    std::cout << *itChoices << std::endl;
                }
                break;
            default:
                std::cout << "tu es le meilleur c'est pas grave" << std::endl;
        }
    }
    p_currentPlayer->setMCharacters(selectedCharacters);
    for (auto p : p_currentPlayer->getMCharacters()) {
        p->setMOwner(p_currentPlayer);
    }

    std::vector<Weapon *> availableWeapons;
    for (auto i : *p_availableWeapons) {
        availableWeapons.push_back(i.getWeaponFromWeaponParser());
    }

    this->chooseWeapons(p_currentPlayer, &availableWeapons);
    if (system("CLS")) system("clear");
    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;
}

void Menu::chooseWeapons(Player *p_currentPlayer, std::vector<Weapon *> *p_availableWeapons) {
    bool done = false;
    char input;
    int cursorCharacters = 0;

    this->m_choices.clear();
    this->m_cursorPosition = 0;

    std::vector<Weapon *> filteredWeapons;

    std::vector<Weapon *>::iterator itWeapon;
    std::vector<Weapon *> c = *p_availableWeapons;

    for (Weapon *weapon : *p_availableWeapons) {
        if (Game::isCharacterCompatibleWithWeapon(p_currentPlayer->getMCharacters().at(cursorCharacters), weapon)) {
            filteredWeapons.push_back(weapon);
        }
    }
    for (itWeapon = filteredWeapons.begin(); itWeapon != filteredWeapons.end(); ++itWeapon) {
        this->m_choices.push_back("  " + (*itWeapon)->getMName() + " (" + Game::getTypeOfWeapon(*itWeapon) + ")");
    }

    do {
        if (system("CLS")) system("clear");
        // BEGIN AFFICHAGE
        std::cout << "Select the weapon that you want to use with " << colors::blue
                  << p_currentPlayer->getMCharacters().at(cursorCharacters)->getMName()
                  << colors::clear
                  << " (Z/S to navigate, I to print info about the weapon, D to confirm) :"
                  << std::endl;
        std::cout << colors::magenta
                  << "-------------------------------------------------------------------------------------------------"
                  << colors::clear
                  << std::endl;

        std::vector<std::string>::iterator itChoices;
        for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
            if (itChoices - this->m_choices.begin() == this->m_cursorPosition) {
                itChoices->at(0) = '>';
                std::cout << colors::orange << *itChoices << colors::clear << std::endl;
            } else {
                itChoices->at(0) = ' ';
                std::cout << *itChoices << std::endl;
            }
        }


        std::cin >> input;

        switch (input) {
            case 'd':
                p_currentPlayer->getMCharacters().at(cursorCharacters)->setMWeapon(
                        filteredWeapons.at(this->m_cursorPosition));
                cursorCharacters++;
                if (cursorCharacters == p_currentPlayer->getMCharacters().size()) break;
                filteredWeapons.clear();
                for (Weapon *weapon : *p_availableWeapons) {
                    if (Game::isCharacterCompatibleWithWeapon(
                            p_currentPlayer->getMCharacters().at(cursorCharacters), weapon)) {
                        filteredWeapons.push_back(weapon);
                    }
                }
                this->m_choices.clear();
                this->m_cursorPosition = 0;
                for (itWeapon = filteredWeapons.begin(); itWeapon != filteredWeapons.end(); ++itWeapon) {
                    this->m_choices.push_back(
                            "  " + (*itWeapon)->getMName() + " (" + Game::getTypeOfWeapon(*itWeapon) + ")");
                }
                break;
            case 'z':
                if (this->m_cursorPosition > 0)
                    this->m_cursorPosition--;
                else
                    this->m_cursorPosition = this->m_choices.size() - 1;
                break;
            case 's':
                if (this->m_cursorPosition < this->m_choices.size() - 1)
                    this->m_cursorPosition++;
                else
                    this->m_cursorPosition = 0;
                break;
            case 'i':
                if (system("CLS")) system("clear");
                this->printWeaponInfo(filteredWeapons.at(this->m_cursorPosition));
                break;
            default:
                std::cout << "Ne t'inqui�tes pas tu es beau" << std::endl;
                break;
        }

        //END AFFICHAGE DEBUT
    } while (cursorCharacters != p_currentPlayer->getMCharacters().size());
    if (system("CLS")) system("clear");
    this->m_cursorPosition = 0;
    this->m_choices.clear();
}


Menu::Menu() {
    this->m_cursorPosition = 0;
}

Menu::~Menu() {

}

/// Method that print the menu to select an action to perform
/// \param p_characterSelected Current character selected in the previous menu
/// \param g //Current game
void Menu::printActionSelectionMenu(Character *p_characterSelected, Game *g) {

    //Clear screen
    if (system("CLS")) system("clear");

    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;

    std::cout << "Select the action to perform with " << p_characterSelected->getMName()
              << " (Z/S to navigate, D to confirm, Q to go back) :" << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;
    if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
        std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                  << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
    else
        std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;

    //Filling choices
    this->m_choices.emplace_back("  Throw a punch");
    std::string resources;
    //Check to print the ressources needed to perform an attack with a weapon
    Weapon *weapon = p_characterSelected->getMWeapon();
    if (Game::getTypeOfCharacter(p_characterSelected) == "Rogue" ||
        Game::getTypeOfCharacter(p_characterSelected) == "Archer") {
        if (Game::getTypeOfWeapon(weapon) == "Dagger") {
            resources = "(" + std::to_string(dynamic_cast<Dagger *>(weapon)->getMDurability()) +
                        " durability left)";
        } else if (Game::getTypeOfWeapon(weapon) == "Bow") {
            resources = "(" + std::to_string(dynamic_cast<Bow *>(weapon)->getMArrows()) +
                        " arrows left)";
        }
    } else if (Game::getTypeOfCharacter(p_characterSelected) == "Warrior") {
        resources = "(" + std::to_string(dynamic_cast<Melee *>(weapon)->getMDurability()) +
                    " durability left)";
    } else if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer") {
        if (Game::getTypeOfWeapon(weapon) == "Staff") {
            resources =
                    "Cost : " + std::to_string(dynamic_cast<Staff *>(weapon)->getMCost()) +
                    " mana";
        } else if (Game::getTypeOfWeapon(weapon) == "Sword") {
            resources = "(" + std::to_string(dynamic_cast<Sword *>(weapon)->getMDurability()) +
                        " durability left)";
        }
    }
    this->m_choices.push_back("  Attack with weapon - " + resources);
    this->m_choices.emplace_back("  Use an ability");

    std::vector<std::string>::iterator itChoices;
    for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
        if (itChoices - this->m_choices.begin() == 0)
            itChoices->at(0) = '>';

        std::cout << *itChoices << std::endl;
    }

    bool choiceCompleted = false;
    char input;

    while (!choiceCompleted) {

        std::cin >> input;

        switch (input) {
            case 'q': //RETURN
                //Clear screen
                if (system("CLS")) system("clear");

                printCharacterSelectionMenuRound(p_characterSelected->getMOwner(), g);
                choiceCompleted = true;
                break;
            case 'z': //UP

                if (system("CLS")) system("clear");

                std::cout << "Select the action to perform with " << p_characterSelected->getMName()
                          << " (Z/S to navigate, D to confirm, Q to go back) :" << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition > 0)
                    this->m_cursorPosition--;
                else
                    this->m_cursorPosition = this->m_choices.size() - 1;

                for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
                    if (itChoices - this->m_choices.begin() == this->m_cursorPosition)
                        itChoices->at(0) = '>';
                    else
                        itChoices->at(0) = ' ';

                    std::cout << *itChoices << std::endl;
                }
                break;
            case 's': //DOWN

                if (system("CLS")) system("clear");

                std::cout << "Select the action to perform with " << p_characterSelected->getMName()
                          << " (Z/S to navigate, D to confirm, Q to go back) :" << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition < this->m_choices.size() - 1)
                    this->m_cursorPosition++;
                else
                    this->m_cursorPosition = 0;

                for (itChoices = this->m_choices.begin(); itChoices != this->m_choices.end(); ++itChoices) {
                    if (itChoices - this->m_choices.begin() == this->m_cursorPosition)
                        itChoices->at(0) = '>';
                    else
                        itChoices->at(0) = ' ';

                    std::cout << *itChoices << std::endl;
                }
                break;
            case 'd': //CONFIRM

                if (system("CLS")) system("clear");

                if (this->m_cursorPosition == 0) {
                    //"Throw a punch" selected
                    choiceCompleted = true;
                    printTargetSelectionForPunch(p_characterSelected, g);

                } else if (this->m_cursorPosition == 1) {
                    //"Attack with weapon" selected
                    //TARGET SELECTION WEAPON
                    choiceCompleted = true;
                    printTargetSelectionForAttackWithWeapon(p_characterSelected, g);

                } else if (this->m_cursorPosition == 2) {
                    //"Use an ability" selected
                    choiceCompleted = true;
                    printAbilitySelectionMenu(p_characterSelected, g);

                }

                break;
            default:
                std::cout << "tu es le meilleur c'est pas grave" << std::endl;
        }
    }
    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;
}

/// Method that print the menu to select a target to punch
/// \param p_characterSelected Character that will punch, selected in the previous menu
/// \param g //Current game
void Menu::printTargetSelectionForPunch(Character *p_characterSelected, Game *g) {
    //"Throw a punch" selected

    //TARGET SELECTION PUNCH

    //Clear screen
    if (system("CLS")) system("clear");

    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;

    std::cout << "Select a target to punch (Z/S to navigate, D to confirm, Q to go back) :" << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;
    if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
        std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                  << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
    else
        std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;

    //Filling choices with targets
    std::vector<Character *>::iterator itEnemies;
    std::vector<Character *> enemies = g->getEnnemyOfCurrentPlayer()->getMCharacters();
    for (itEnemies = enemies.begin(); itEnemies != enemies.end(); ++itEnemies) {
        this->m_choices.push_back(
                "  " + (*itEnemies)->getMName() + " - " + std::to_string((*itEnemies)->getMLife()) + " HP");
    }

    std::vector<Character *>::iterator itAllies;
    std::vector<Character *> allies = g->getMCurrentPlayer()->getMCharacters();
    for (itAllies = allies.begin(); itAllies != allies.end(); ++itAllies) {
        this->m_choices.push_back(
                "  " + (*itAllies)->getMName() + " - " + std::to_string((*itAllies)->getMLife()) + " HP");
    }

    //Printing choices menu
    std::vector<std::string>::iterator itChoicesTarget;
    for (itChoicesTarget = this->m_choices.begin();
         itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
        //Check if pointed character is dead
        while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
            this->m_cursorPosition++;
        }


        if (itChoicesTarget - this->m_choices.begin() == 0) {
            std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
        } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
            std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
        }

        if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
            itChoicesTarget->at(0) = '>';
            if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
            } else {
                std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
            }
        } else {
            std::cout << *itChoicesTarget << std::endl;
        }
    }

    //Override previous inputs
    char inputTarget;

    bool choiceCompleted = false;

    while (!choiceCompleted) {

        std::cin >> inputTarget;

        switch (inputTarget) {
            case 'q': //GO BACK
                if (system("CLS")) system("clear");
                printActionSelectionMenu(p_characterSelected, g);
                choiceCompleted = true;
                break;

            case 'z':
                if (system("CLS")) system("clear");

                std::cout << "Select a target to punch (Z/S to navigate, D to confirm, Q to go back) :" << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition > 0)
                    this->m_cursorPosition--;
                else
                    this->m_cursorPosition = this->m_choices.size() - 1;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == 0)
                        this->m_cursorPosition = this->m_choices.size() - 1;
                    else
                        this->m_cursorPosition--;
                }

                for (itChoicesTarget = this->m_choices.begin();
                     itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
                    //Check if pointed character is dead
                    while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                        this->m_cursorPosition++;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == 0) {
                        std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
                    } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
                        std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoicesTarget->at(0) = '>';
                        if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                            itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                            std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
                        } else {
                            std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
                        }
                    } else {
                        itChoicesTarget->at(0) = ' ';
                        std::cout << *itChoicesTarget << std::endl;
                    }
                }
                break;
            case 's': //DOWN

                if (system("CLS")) system("clear");

                std::cout << "Select a target to punch (Z/S to navigate, D to confirm, Q to go back) :" << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition < this->m_choices.size() - 1)
                    this->m_cursorPosition++;
                else
                    this->m_cursorPosition = 0;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == 0)
                        this->m_cursorPosition = this->m_choices.size() - 1;
                    else
                        this->m_cursorPosition++;
                }

                for (itChoicesTarget = this->m_choices.begin();
                     itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
                    //Check if pointed character is dead
                    while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                        this->m_cursorPosition++;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == 0) {
                        std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
                    } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
                        std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoicesTarget->at(0) = '>';
                        if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                            itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                            std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
                        } else {
                            std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
                        }
                    } else {
                        itChoicesTarget->at(0) = ' ';
                        std::cout << *itChoicesTarget << std::endl;
                    }
                }
                break;

            case 'd':
                Character *target;
                if (this->m_cursorPosition < g->getEnnemyOfCurrentPlayer()->getMCharacters().size()) {
                    //AN ENEMY IS THE TARGET
                    target = g->getEnnemyOfCurrentPlayer()->getMCharacters().at(this->m_cursorPosition);
                } else {
                    //AN ALLY IS THE TARGET
                    target = g->getMCurrentPlayer()->getMCharacters().at(this->m_cursorPosition -
                                                                         g->getEnnemyOfCurrentPlayer()->getMCharacters().size());
                }
                if (system("CLS")) system("clear");
                try {
                    p_characterSelected->punch(target);
                    choiceCompleted = true;
                    break;
                }
                catch (VanishException &vanishException) {
                    std::cout << colors::blue << p_characterSelected->getMName() << colors::red << " cannot attack " <<
                              colors::blue << target->getMName() << colors::red << " : "
                              << vanishException.what()
                              << colors::clear
                              << std::endl;
                }

                std::cout << "Select a target to punch (Z/S to navigate, D to confirm, Q to go back) :" << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == 0)
                        this->m_cursorPosition = this->m_choices.size() - 1;
                    else
                        this->m_cursorPosition--;
                }

                for (itChoicesTarget = this->m_choices.begin();
                     itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
                    //Check if pointed character is dead
                    while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                        this->m_cursorPosition++;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == 0) {
                        std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
                    } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
                        std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoicesTarget->at(0) = '>';
                        if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                            itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                            std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
                        } else {
                            std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
                        }
                    } else {
                        itChoicesTarget->at(0) = ' ';
                        std::cout << *itChoicesTarget << std::endl;
                    }
                }
                break;
            default:
                std::cout << "TU ES BEAU MEC " << std::endl;
        }
    }
    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;
}

/// Method that print the menu to select an ability to use
/// \param p_characterSelected Character selected in the previous menu
/// \param g //Current game
void Menu::printAbilitySelectionMenu(Character *p_characterSelected, Game *g) {
    //Refresh screen
    if (system("CLS")) system("clear");

    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;

    std::cout << "Select an ability (Z/S to navigate, D to confirm, Q to go back) :" << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;
    if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
        std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                  << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
    else
        std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;

    //Filling choices
    std::vector<std::string>::iterator itAbilities;
    std::vector<std::string> c = p_characterSelected->getAbilities();
    for (itAbilities = c.begin(); itAbilities != c.end(); ++itAbilities) {
        this->m_choices.push_back("  " + (*itAbilities));
    }

    //Printing choices menu
    std::vector<std::string>::iterator itChoicesAbilities;
    for (itChoicesAbilities = this->m_choices.begin();
         itChoicesAbilities != this->m_choices.end(); ++itChoicesAbilities) {
        if (itChoicesAbilities - this->m_choices.begin() == 0)
            itChoicesAbilities->at(0) = '>';

        std::cout << *itChoicesAbilities << std::endl;
    }

    //Override previous input
    char inputAbility;
    bool choiceCompleted = false;

    while (!choiceCompleted) {

        std::cin >> inputAbility;

        switch (inputAbility) {
            case 'q': //GO BACK
                if (system("CLS")) system("clear");
                printActionSelectionMenu(p_characterSelected, g);
                choiceCompleted = true;
                break;

            case 'z':
                if (system("CLS")) system("clear");

                std::cout << "Select an ability (Z/S to navigate, D to confirm, Q to go back) :"
                          << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition > 0)
                    this->m_cursorPosition--;
                else
                    this->m_cursorPosition = this->m_choices.size() - 1;

                for (itChoicesAbilities = this->m_choices.begin();
                     itChoicesAbilities != this->m_choices.end(); ++itChoicesAbilities) {
                    if (itChoicesAbilities - this->m_choices.begin() == this->m_cursorPosition)
                        itChoicesAbilities->at(0) = '>';
                    else
                        itChoicesAbilities->at(0) = ' ';

                    std::cout << *itChoicesAbilities << std::endl;
                }
                break;
            case 's': //DOWN

                if (system("CLS")) system("clear");

                std::cout << "Select an ability (Z/S to navigate, D to confirm, Q to go back) :"
                          << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition < this->m_choices.size() - 1)
                    this->m_cursorPosition++;
                else
                    this->m_cursorPosition = 0;

                for (itChoicesAbilities = this->m_choices.begin();
                     itChoicesAbilities != this->m_choices.end(); ++itChoicesAbilities) {
                    if (itChoicesAbilities - this->m_choices.begin() == this->m_cursorPosition)
                        itChoicesAbilities->at(0) = '>';
                    else
                        itChoicesAbilities->at(0) = ' ';

                    std::cout << *itChoicesAbilities << std::endl;
                }
                break;
            case 'd': //CONFIRM

                //Clear screen
                if (system("CLS")) system("clear");

                //Check if the capacity requires a target
                if (this->m_choices.at(this->m_cursorPosition).find("Aim") != std::string::npos) {
                    //The selected capacity is Aim
                    try {
                        dynamic_cast<Archer *>(p_characterSelected)->aimOn();
                        choiceCompleted = true;
                        break;
                    }
                    catch (AimException &aimException) {
                        std::cout << colors::blue << p_characterSelected->getMName() << colors::red
                                  << " cannot aim : "
                                  << aimException.what()
                                  << colors::clear
                                  << std::endl;
                    }
                } else if (this->m_choices.at(this->m_cursorPosition).find("Vanish") !=
                           std::string::npos) {
                    //The selected capacity is Vanish
                    try {
                        dynamic_cast<Rogue *>(p_characterSelected)->vanish();
                        choiceCompleted = true;
                        break;
                    }
                    catch (VanishException &vanishException) {
                        std::cout << colors::blue << p_characterSelected->getMName() << colors::red
                                  << " cannot vanish : "
                                  << vanishException.what()
                                  << colors::clear
                                  << std::endl;
                    }
                } else if (this->m_choices.at(this->m_cursorPosition).find("Shout") !=
                           std::string::npos) {
                    //The selected capacity is Shout
                    dynamic_cast<Warrior *>(p_characterSelected)->shout();
                    choiceCompleted = true;
                } else if (this->m_choices.at(this->m_cursorPosition).find("Regenerate mana") !=
                           std::string::npos) {
                    //The selected capacity is Regenerate mana
                    try {
                        dynamic_cast<Sorcerer *>(p_characterSelected)->regenerate();
                        choiceCompleted = true;
                        break;
                    }
                    catch (ManaException &manaException) {
                        std::cout << colors::blue << p_characterSelected->getMName() << colors::red
                                  << " cannot regenerate the mana of " <<
                                  colors::blue << p_characterSelected->getMName() << colors::red << " : "
                                  << manaException.what()
                                  << colors::clear
                                  << std::endl;
                    }
                } else {
                    //The ability requires a target : TARGET SELECTION
                    printTargetSelectionForAbility(p_characterSelected, g);
                    choiceCompleted = true;
                    break;
                }
                std::cout << "Select an ability (Z/S to navigate, D to confirm, Q to go back) :"
                          << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                for (itChoicesAbilities = this->m_choices.begin();
                     itChoicesAbilities != this->m_choices.end(); ++itChoicesAbilities) {
                    if (itChoicesAbilities - this->m_choices.begin() == this->m_cursorPosition)
                        itChoicesAbilities->at(0) = '>';
                    else
                        itChoicesAbilities->at(0) = ' ';

                    std::cout << *itChoicesAbilities << std::endl;
                }
                break;
            default:
                std::cout << "TU ES STOCK PTN " << std::endl;
        }
    }
    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;
}

/// Method that print the menu to select a target to use an ability on
/// \param p_characterSelected Character that will play, selected in the previous menu
/// \param g //Current game
void Menu::printTargetSelectionForAbility(Character *p_characterSelected, Game *g) {
    //The ability requires a target : TARGET SELECTION

    std::string abilityChoosed = this->m_choices.at(this->m_cursorPosition);

    //Clear screen
    if (system("CLS")) system("clear");

    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;

    std::cout << "Select a target to "
              << abilityChoosed
              << " (Z/S to navigate, D to confirm, Q to go back) :"
              << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;
    if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
        std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                  << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
    else
        std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;

    //Filling choices with targets
    std::vector<Character *>::iterator itEnemies;
    std::vector<Character *> enemies = g->getEnnemyOfCurrentPlayer()->getMCharacters();
    for (itEnemies = enemies.begin(); itEnemies != enemies.end(); ++itEnemies) {
        this->m_choices.push_back(
                "  " + (*itEnemies)->getMName() + " - " + std::to_string((*itEnemies)->getMLife()) + " HP");
    }

    std::vector<Character *>::iterator itAllies;
    std::vector<Character *> allies = g->getMCurrentPlayer()->getMCharacters();
    for (itAllies = allies.begin(); itAllies != allies.end(); ++itAllies) {
        this->m_choices.push_back(
                "  " + (*itAllies)->getMName() + " - " + std::to_string((*itAllies)->getMLife()) + " HP");
    }

    //Printing choices menu
    std::vector<std::string>::iterator itChoicesTarget;
    for (itChoicesTarget = this->m_choices.begin(); itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
        //Check if pointed character is dead
        while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
            this->m_cursorPosition++;
        }
        if (itChoicesTarget - this->m_choices.begin() == 0) {
            std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
        } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
            std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
        }

        if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
            itChoicesTarget->at(0) = '>';
            if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
            } else {
                std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
            }
        } else {
            std::cout << *itChoicesTarget << std::endl;
        }
    }

    //Override previous inputs
    char inputTarget;
    bool choiceCompleted = false;

    while (!choiceCompleted) {

        std::cin >> inputTarget;

        switch (inputTarget) {
            case 'q': //GO BACK
                if (system("CLS")) system("clear");
                printAbilitySelectionMenu(p_characterSelected, g);
                choiceCompleted = true;
                break;

            case 'z':
                if (system("CLS")) system("clear");

                std::cout << "Select a target to "
                          << abilityChoosed
                          << " (Z/S to navigate, D to confirm, Q to go back) :"
                          << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition > 0)
                    this->m_cursorPosition--;
                else
                    this->m_cursorPosition = this->m_choices.size() - 1;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == 0)
                        this->m_cursorPosition = this->m_choices.size() - 1;
                    else
                        this->m_cursorPosition--;
                }

                for (itChoicesTarget = this->m_choices.begin();
                     itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
                    //Check if pointed character is dead
                    while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                        this->m_cursorPosition++;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == 0) {
                        std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
                    } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
                        std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoicesTarget->at(0) = '>';
                        if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                            itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                            std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
                        } else {
                            std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
                        }
                    } else {
                        itChoicesTarget->at(0) = ' ';
                        std::cout << *itChoicesTarget << std::endl;
                    }
                }
                break;
            case 's': //DOWN

                if (system("CLS")) system("clear");

                std::cout << "Select a target to "
                          << abilityChoosed
                          << " (Z/S to navigate, D to confirm, Q to go back) :"
                          << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition < this->m_choices.size() - 1)
                    this->m_cursorPosition++;
                else
                    this->m_cursorPosition = 0;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == this->m_choices.size() - 1)
                        this->m_cursorPosition = 0;
                    else
                        this->m_cursorPosition++;
                }

                for (itChoicesTarget = this->m_choices.begin();
                     itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
                    //Check if pointed character is dead
                    while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                        this->m_cursorPosition++;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == 0) {
                        std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
                    } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
                        std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoicesTarget->at(0) = '>';
                        if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                            itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                            std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
                        } else {
                            std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
                        }
                    } else {
                        itChoicesTarget->at(0) = ' ';
                        std::cout << *itChoicesTarget << std::endl;
                    }
                }
                break;

            case 'd':
                if (system("CLS")) system("clear");
                Character *target;
                if (this->m_cursorPosition <
                    g->getEnnemyOfCurrentPlayer()->getMCharacters().size()) {
                    //AN ENEMY IS THE TARGET
                    target = g->getEnnemyOfCurrentPlayer()->getMCharacters().at(
                            this->m_cursorPosition);
                } else {
                    //AN ALLY IS THE TARGET
                    target = g->getMCurrentPlayer()->getMCharacters().at(
                            this->m_cursorPosition -
                            g->getEnnemyOfCurrentPlayer()->getMCharacters().size());
                }

                //Searching capacity
                if (abilityChoosed.find("Repair") !=
                    std::string::npos) {
                    //The selected capacity is Repair
                    dynamic_cast<Warrior *>(p_characterSelected)->repair(
                            target->getMWeapon());
                    choiceCompleted = true;
                    break;
                } else if (abilityChoosed.find("Cure") !=
                           std::string::npos) {
                    //The selected capacity is Cure
                    try {
                        dynamic_cast<Archer *>(p_characterSelected)->antidote(target);
                        choiceCompleted = true;
                        break;
                    }
                    catch (AntidoteException &antidoteException) {
                        std::cout << colors::red << "Cannot cure " << colors::blue << target->getMName() << colors::red
                                  << " from poison : "
                                  << antidoteException.what() << colors::clear << std::endl;
                    }
                } else if (abilityChoosed.find("Heal") !=
                           std::string::npos) {
                    //The selected capacity is Heal
                    try {
                        dynamic_cast<Sorcerer *>(p_characterSelected)->heal(target);
                        choiceCompleted = true;
                        break;
                    }
                    catch (HealException &healException) {
                        std::cout << colors::red << "Cannot heal " << colors::blue << target->getMName() << colors::red
                                  << " : "
                                  << healException.what() << colors::clear << std::endl;
                    }
                } else if (abilityChoosed.find(
                        "Enchant weapon") != std::string::npos) {
                    //The selected capacity is Enchant weapon
                    if (Game::getTypeOfWeapon(target->getMWeapon()) == "Dagger" ||
                        Game::getTypeOfWeapon(target->getMWeapon()) == "Sword") {
                        dynamic_cast<Sorcerer *>(p_characterSelected)->enchant(
                                target->getMWeapon());
                        choiceCompleted = true;
                        break;
                    } else {
                        std::cout << colors::red << "This weapon cannot be enchanted" << colors::clear << std::endl;
                    }
                } else {
                    std::cout << "NOT FOUND" << std::endl; //DEBUG
                }
                std::cout << "Select a target to "
                          << abilityChoosed
                          << " (Z/S to navigate, D to confirm, Q to go back) :"
                          << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == this->m_choices.size() - 1)
                        this->m_cursorPosition = 0;
                    else
                        this->m_cursorPosition++;
                }


                for (itChoicesTarget = this->m_choices.begin();
                     itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
                    //Check if pointed character is dead
                    while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                        this->m_cursorPosition++;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == 0) {
                        std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
                    } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
                        std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoicesTarget->at(0) = '>';
                        if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                            itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                            std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
                        } else {
                            std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
                        }
                    } else {
                        itChoicesTarget->at(0) = ' ';
                        std::cout << *itChoicesTarget << std::endl;
                    }
                }
                break;
            default:
                std::cout << "TU ES SEXY MEC " << std::endl;
        }
    }
    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;
}

/// Method that print the menu to select a target to attack with a weapon
/// \param p_characterSelected Character that will attack, selected in the previous menu
/// \param g //Current game
void Menu::printTargetSelectionForAttackWithWeapon(Character *p_characterSelected, Game *g) {
    //Clear screen
    if (system("CLS")) system("clear");

    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;

    std::cout << "Select a target to attack with your weapon (Z/S to navigate, D to confirm, Q to go back) :"
              << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;
    if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
        std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                  << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
    else
        std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;

    //Filling choices with targets
    std::vector<Character *>::iterator itEnemies;
    std::vector<Character *> enemies = g->getEnnemyOfCurrentPlayer()->getMCharacters();
    for (itEnemies = enemies.begin(); itEnemies != enemies.end(); ++itEnemies) {
        this->m_choices.push_back(
                "  " + (*itEnemies)->getMName() + " - " + std::to_string((*itEnemies)->getMLife()) + " HP");
    }

    std::vector<Character *>::iterator itAllies;
    std::vector<Character *> allies = g->getMCurrentPlayer()->getMCharacters();
    for (itAllies = allies.begin(); itAllies != allies.end(); ++itAllies) {
        this->m_choices.push_back(
                "  " + (*itAllies)->getMName() + " - " + std::to_string((*itAllies)->getMLife()) + " HP");
    }

    //Printing choices menu
    std::vector<std::string>::iterator itChoicesTarget;
    for (itChoicesTarget = this->m_choices.begin(); itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
        //Check if pointed character is dead
        while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
            this->m_cursorPosition++;
        }

        if (itChoicesTarget - this->m_choices.begin() == 0) {
            std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
        } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
            std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
        }

        if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
            itChoicesTarget->at(0) = '>';
            if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
            } else {
                std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
            }
        } else {
            std::cout << *itChoicesTarget << std::endl;
        }
    }

    //Override previous inputs
    char inputTarget;
    bool choiceCompleted = false;
    while (!choiceCompleted) {

        std::cin >> inputTarget;

        switch (inputTarget) {
            case 'q': //GO BACK
                if (system("CLS")) system("clear");
                printActionSelectionMenu(p_characterSelected, g);
                choiceCompleted = true;
                break;
            case 'z': //UP
                if (system("CLS")) system("clear");

                std::cout
                        << "Select a target to attack with your weapon (Z/S to navigate, D to confirm, Q to go back) :"
                        << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition > 0)
                    this->m_cursorPosition--;
                else
                    this->m_cursorPosition = this->m_choices.size() - 1;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == 0)
                        this->m_cursorPosition = this->m_choices.size() - 1;
                    else
                        this->m_cursorPosition--;
                }

                for (itChoicesTarget = this->m_choices.begin();
                     itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
                    //Check if pointed character is dead
                    while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                        this->m_cursorPosition++;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == 0) {
                        std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
                    } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
                        std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoicesTarget->at(0) = '>';
                        if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                            itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                            std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
                        } else {
                            std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
                        }
                    } else {
                        itChoicesTarget->at(0) = ' ';
                        std::cout << *itChoicesTarget << std::endl;
                    }
                }
                break;
            case 's': //DOWN

                if (system("CLS")) system("clear");

                std::cout
                        << "Select a target to attack with your weapon (Z/S to navigate, D to confirm, Q to go back) :"
                        << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                if (this->m_cursorPosition < this->m_choices.size() - 1)
                    this->m_cursorPosition++;
                else
                    this->m_cursorPosition = 0;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == this->m_choices.size() - 1)
                        this->m_cursorPosition = 0;
                    else
                        this->m_cursorPosition++;
                }

                for (itChoicesTarget = this->m_choices.begin();
                     itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
                    //Check if pointed character is dead
                    while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                        this->m_cursorPosition++;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == 0) {
                        std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
                    } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
                        std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoicesTarget->at(0) = '>';
                        if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                            itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                            std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
                        } else {
                            std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
                        }
                    } else {
                        itChoicesTarget->at(0) = ' ';
                        std::cout << *itChoicesTarget << std::endl;
                    }
                }
                break;

            case 'd':
                Character *target;
                if (this->m_cursorPosition < g->getEnnemyOfCurrentPlayer()->getMCharacters().size()) {
                    //AN ENEMY IS THE TARGET
                    target = g->getEnnemyOfCurrentPlayer()->getMCharacters().at(this->m_cursorPosition);
                } else {
                    //AN ALLY IS THE TARGET
                    target = g->getMCurrentPlayer()->getMCharacters().at(this->m_cursorPosition -
                                                                         g->getEnnemyOfCurrentPlayer()->getMCharacters().size());
                }
                try {
                    p_characterSelected->attackWithWeapon(target);
                    choiceCompleted = true;
                    break;
                }
                catch (ManaException &manaException) {
                    std::cout << colors::blue << p_characterSelected->getMName() << colors::red << " cannot attack " <<
                              colors::blue << target->getMName() << colors::red << " : "
                              << manaException.what()
                              << colors::clear
                              << std::endl;
                }
                catch (ArrowsException &arrowsException) {
                    std::cout << colors::blue << p_characterSelected->getMName() << colors::red << " cannot attack " <<
                              colors::blue << target->getMName() << colors::red << " : "
                              << arrowsException.what()
                              << colors::clear
                              << std::endl;
                }
                catch (VanishException &vanishException) {
                    std::cout << colors::blue << p_characterSelected->getMName() << colors::red << " cannot attack " <<
                              colors::blue << target->getMName() << colors::red << " : "
                              << vanishException.what()
                              << colors::clear
                              << std::endl;
                }

                std::cout
                        << "Select a target to attack with your weapon (Z/S to navigate, D to confirm, Q to go back) :"
                        << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;
                if (Game::getTypeOfCharacter(p_characterSelected) == "Sorcerer")
                    std::cout << "HP : " << p_characterSelected->getMLife() << " | Mana : "
                              << dynamic_cast<Sorcerer *>(p_characterSelected)->getMMana() << std::endl;
                else
                    std::cout << "HP : " << p_characterSelected->getMLife() << std::endl;
                std::cout << colors::magenta
                          << "-------------------------------------------------------------------------------------------------"
                          << colors::clear
                          << std::endl;

                //Check if the pointed character is dead
                while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                    if (this->m_cursorPosition == 0)
                        this->m_cursorPosition = this->m_choices.size() - 1;
                    else
                        this->m_cursorPosition--;
                }

                for (itChoicesTarget = this->m_choices.begin();
                     itChoicesTarget != this->m_choices.end(); ++itChoicesTarget) {
                    //Check if pointed character is dead
                    while (this->m_choices.at(this->m_cursorPosition).find("DEAD") != std::string::npos) {
                        this->m_cursorPosition++;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == 0) {
                        std::cout << colors::red << "ENEMIES :" << colors::clear << std::endl;
                    } else if (itChoicesTarget - this->m_choices.begin() == enemies.size()) {
                        std::cout << colors::green << "ALLIES :" << colors::clear << std::endl;
                    }

                    if (itChoicesTarget - this->m_choices.begin() == this->m_cursorPosition) {
                        itChoicesTarget->at(0) = '>';
                        if (itChoicesTarget - this->m_choices.begin() >= 0 &&
                            itChoicesTarget - this->m_choices.begin() < enemies.size()) {
                            std::cout << colors::red << *itChoicesTarget << colors::clear << std::endl;
                        } else {
                            std::cout << colors::green << *itChoicesTarget << colors::clear << std::endl;
                        }
                    } else {
                        itChoicesTarget->at(0) = ' ';
                        std::cout << *itChoicesTarget << std::endl;
                    }
                }
                break;
            default:
                std::cout << "TU ES INTELLIGENT MEC " << std::endl;
        }
    }
    //Choices reset
    this->m_choices.clear();
    this->m_cursorPosition = 0;
}

void Menu::choosePlayers(Game *p_game) {
    std::string J1, J2;
    if (system("CLS")) system("clear");
    std::cout << colors::blue << R"(
____/\\\\\\\\\______/\\\\\\\\\\\\\_______/\\\\\\\\\\\\_
 __/\\\///////\\\___\/\\\/////////\\\___/\\\//////////__
  _\/\\\_____\/\\\___\/\\\_______\/\\\__/\\\_____________
   _\/\\\\\\\\\\\/____\/\\\\\\\\\\\\\/__\/\\\____/\\\\\\\_
    _\/\\\//////\\\____\/\\\/////////____\/\\\___\/////\\\_
     _\/\\\____\//\\\___\/\\\_____________\/\\\_______\/\\\_
      _\/\\\_____\//\\\__\/\\\_____________\/\\\_______\/\\\_
       _\/\\\______\//\\\_\/\\\_____________\//\\\\\\\\\\\\/__
        _\///________\///__\///_______________\////////////____
)" << colors::clear << std::endl;
    std::cout << "Enter the name of the \033[1;32mplayer 1\033[1;0m : ";
    std::getline(std::cin, J1);
    std::cout << "Enter the name of the \033[1;32mplayer 2\033[1;0m: ";
    std::getline(std::cin, J2);
    Player *player1, *player2;

    player1 = new Player(J1);
    player2 = new Player(J2);

    p_game->setMPlayer1(player1);
    p_game->setMPlayer2(player2);

    if (system("CLS")) system("clear");
}

void Menu::printCharacterInfo(Character *p_c) {
    char input;
    bool quit = false;
    if (system("CLS")) system("clear");

    std::cout << "Characteristics of \033[1;34m" << p_c->getMName() << "\033[1;0m (Q to go back) :"
              << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;

    std::cout << "Name             : \t" << p_c->getMName() << std::endl;
    std::cout << "Class            : \t" << Game::getTypeOfCharacter(p_c) << std::endl;
    std::cout << "Life             : \t";
    if (p_c->getMLife() <= 163) {
        std::cout << colors::red;
    } else if (p_c->getMLife() > 163 && p_c->getMLife() <= 201) {
        std::cout << colors::orange;
    } else {
        std::cout << colors::green;
    }
    std::cout << p_c->getMLife() << colors::clear << std::endl;
    std::cout << "Attack           : \t";
    if (p_c->getMAttack() <= 163) {
        std::cout << colors::red;
    } else if (p_c->getMAttack() > 163 && p_c->getMAttack() <= 201) {
        std::cout << colors::green;
    } else {
        std::cout << colors::green;
    }
    std::cout << p_c->getMAttack() << colors::clear << std::endl;
    std::cout << "Defense          : \t";
    if (p_c->getMDefense() <= 20) {
        std::cout << colors::red;
    } else if (p_c->getMDefense() > 20 && p_c->getMDefense() <= 28) {
        std::cout << colors::green;
    } else {
        std::cout << colors::green;
    }
    std::cout << p_c->getMDefense() << colors::clear << std::endl;
    std::cout << "Intelligence     : \t";
    if (p_c->getMIntelligence() <= 12) {
        std::cout << colors::red;
    } else if (p_c->getMIntelligence() > 12 && p_c->getMIntelligence() <= 15) {
        std::cout << colors::green;
    } else {
        std::cout << colors::green;
    }
    std::cout << p_c->getMIntelligence() << colors::clear << std::endl;
    std::cout << "Dodge            : \t";
    if (p_c->getMDodge() <= 0.16) {
        std::cout << colors::red;
    } else if (p_c->getMDodge() > 0.16 && p_c->getMDodge() <= 0.20) {
        std::cout << colors::green;
    } else {
        std::cout << colors::green;
    }
    std::cout << p_c->getMDodge() << colors::clear << std::endl;
    std::cout << "Speed            : \t";
    if (p_c->getMSpeed() <= 21) {
        std::cout << colors::red;
    } else if (p_c->getMSpeed() > 21 && p_c->getMSpeed() <= 24) {
        std::cout << colors::green;
    } else {
        std::cout << colors::green;
    }
    std::cout << p_c->getMSpeed() << colors::clear << std::endl;
    std::cout << "Immune to poison : \t"
              << (p_c->getMIsPoisoned() == -1 ? (colors::green + "YES") : (colors::red + "NO"))
              << colors::clear
              << std::endl;


    while (!quit) {
        std::cin >> input;

        if (input == 'q' || input == 'Q') {
            quit = true;
            break;
        } else {
            if (system("CLS")) system("clear");
            std::cout << "Characteristics of \033[1;34m" << p_c->getMName() << "\033[1;0m (Q to go back) :"
                      << std::endl;
            std::cout << colors::magenta
                      << "-------------------------------------------------------------------------------------------------"
                      << colors::clear
                      << std::endl;

            std::cout << "Name             : \t" << p_c->getMName() << std::endl;
            std::cout << "Class            : \t" << Game::getTypeOfCharacter(p_c) << std::endl;
            std::cout << "Life             : \t";
            if (p_c->getMLife() <= 163) {
                std::cout << colors::red;
            } else if (p_c->getMLife() > 163 && p_c->getMLife() <= 201) {
                std::cout << colors::orange;
            } else {
                std::cout << colors::green;
            }
            std::cout << p_c->getMLife() << colors::clear << std::endl;
            std::cout << "Attack           : \t";
            if (p_c->getMAttack() <= 163) {
                std::cout << colors::red;
            } else if (p_c->getMAttack() > 163 && p_c->getMAttack() <= 201) {
                std::cout << colors::green;
            } else {
                std::cout << colors::green;
            }
            std::cout << p_c->getMAttack() << colors::clear << std::endl;
            std::cout << "Defense          : \t";
            if (p_c->getMDefense() <= 20) {
                std::cout << colors::red;
            } else if (p_c->getMDefense() > 20 && p_c->getMDefense() <= 28) {
                std::cout << colors::green;
            } else {
                std::cout << colors::green;
            }
            std::cout << p_c->getMDefense() << colors::clear << std::endl;
            std::cout << "Intelligence     : \t";
            if (p_c->getMIntelligence() <= 12) {
                std::cout << colors::red;
            } else if (p_c->getMIntelligence() > 12 && p_c->getMIntelligence() <= 15) {
                std::cout << colors::green;
            } else {
                std::cout << colors::green;
            }
            std::cout << p_c->getMIntelligence() << colors::clear << std::endl;
            std::cout << "Dodge            : \t";
            if (p_c->getMDodge() <= 0.16) {
                std::cout << colors::red;
            } else if (p_c->getMDodge() > 0.16 && p_c->getMDodge() <= 0.20) {
                std::cout << colors::green;
            } else {
                std::cout << colors::green;
            }
            std::cout << p_c->getMDodge() << colors::clear << std::endl;
            std::cout << "Speed            : \t";
            if (p_c->getMSpeed() <= 21) {
                std::cout << colors::red;
            } else if (p_c->getMSpeed() > 21 && p_c->getMSpeed() <= 24) {
                std::cout << colors::green;
            } else {
                std::cout << colors::green;
            }
            std::cout << p_c->getMSpeed() << colors::clear << std::endl;
            std::cout << "Immune to poison : \t"
                      << (p_c->getMIsPoisoned() == -1 ? (colors::green + "YES") : (colors::red + "NO"))
                      << colors::clear
                      << std::endl;
        }
    }
    if (system("CLS")) system("clear");
}

void Menu::printWeaponInfo(Weapon *p_weapon) {
    char input;
    bool quit = false;
    if (system("CLS")) system("clear");

    std::cout << "Characteristics of \033[1;34m" << p_weapon->getMName() << colors::clear << " (Q to go back) :"
              << std::endl;
    std::cout << colors::magenta
              << "-------------------------------------------------------------------------------------------------"
              << colors::clear
              << std::endl;

    std::cout << "Name             : \t" << p_weapon->getMName() << std::endl;
    std::cout << "Type             : \t" << Game::getTypeOfWeapon(p_weapon) << std::endl;
    std::cout << "Critical         : \t";
    if (p_weapon->getMCritical() <= 0.14) {
        std::cout << colors::red;
    } else if (p_weapon->getMCritical() > 0.14 && p_weapon->getMCritical() <= 0.19) {
        std::cout << colors::orange;
    } else {
        std::cout << colors::green;
    }
    std::cout << p_weapon->getMCritical() << colors::clear << std::endl;
    std::cout << "Damage           : \t";
    if (p_weapon->getMDamage() <= 10) {
        std::cout << colors::red;
    } else if (p_weapon->getMDamage() > 10 && p_weapon->getMDamage() <= 13) {
        std::cout << colors::orange;
    } else {
        std::cout << colors::green;
    }
    std::cout << p_weapon->getMDamage() << colors::clear << std::endl;

    if (Game::getTypeOfWeapon(p_weapon) == "Staff") {
        std::cout << "Cost             : \t" << dynamic_cast<Staff *>(p_weapon)->getMCost() << " Mana/hit" << std::endl;
    } else if (Game::getTypeOfWeapon(p_weapon) == "Sword") {
        std::cout << "Durability       : \t" << dynamic_cast<Sword *>(p_weapon)->getMDurability() << std::endl;
    } else if (Game::getTypeOfWeapon(p_weapon) == "Dagger") {
        std::cout << "Durability       : \t" << dynamic_cast<Dagger *>(p_weapon)->getMDurability() << std::endl;
    } else if (Game::getTypeOfWeapon(p_weapon) == "Bow") {
        std::cout << "Arrows           : \t" << dynamic_cast<Bow *>(p_weapon)->getMArrows() << std::endl;
    }
    std::cout << "Bonuses          : \t" << std::endl;

    std::map<std::string, float> bonuses = p_weapon->getMBonus();
    if (bonuses.find("hp") != bonuses.end()) {
        if (bonuses.at("hp") > 0) {
            std::cout << "\t +" << bonuses.at("hp") << " HP" << std::endl;
        } else if (bonuses.at("hp") < 0) {
            std::cout << "\t " << bonuses.at("hp") << " HP" << std::endl;
        }
    }

    if (bonuses.find("intelligence") != bonuses.end()) {
        if (bonuses.at("intelligence") > 0) {
            std::cout << "\t +" << bonuses.at("intelligence") << " Intelligence" << std::endl;
        } else if (bonuses.at("intelligence") < 0) {
            std::cout << "\t " << bonuses.at("intelligence") << " Intelligence" << std::endl;
        }
    }
    if (bonuses.find("attack") != bonuses.end()) {
        if (bonuses.at("attack") > 0) {
            std::cout << "\t +" << bonuses.at("attack") << " Attack" << std::endl;
        } else if (bonuses.at("attack") < 0) {
            std::cout << "\t " << bonuses.at("attack") << " Attack" << std::endl;
        }
    }

    if (bonuses.find("agilite") != bonuses.end()) {
        if (bonuses.at("agilite") > 0) {
            std::cout << "\t +" << bonuses.at("agilite") << " Agility" << std::endl;
        } else if (bonuses.at("agilite") < 0) {
            std::cout << "\t " << bonuses.at("agilite") << " Agility" << std::endl;
        }
    }

    while (!quit) {
        std::cin >> input;

        if (input == 'q' || input == 'Q') {
            quit = true;
            break;
        } else {
            if (system("CLS")) system("clear");
            std::cout << "Characteristics of \033[1;34m" << p_weapon->getMName() << colors::clear << " (Q to go back) :"
                      << std::endl;
            std::cout << colors::magenta
                      << "-------------------------------------------------------------------------------------------------"
                      << colors::clear
                      << std::endl;

            std::cout << "Name             : \t" << p_weapon->getMName() << std::endl;
            std::cout << "Type             : \t" << Game::getTypeOfWeapon(p_weapon) << std::endl;
            std::cout << "Critical         : \t";
            if (p_weapon->getMCritical() <= 0.14) {
                std::cout << colors::red;
            } else if (p_weapon->getMCritical() > 0.14 && p_weapon->getMCritical() <= 0.19) {
                std::cout << colors::orange;
            } else {
                std::cout << colors::green;
            }
            std::cout << p_weapon->getMCritical() << colors::clear << std::endl;
            std::cout << "Damage           : \t";
            if (p_weapon->getMDamage() <= 10) {
                std::cout << colors::red;
            } else if (p_weapon->getMDamage() > 10 && p_weapon->getMDamage() <= 13) {
                std::cout << colors::orange;
            } else {
                std::cout << colors::green;
            }
            std::cout << p_weapon->getMDamage() << colors::clear << std::endl;

            if (Game::getTypeOfWeapon(p_weapon) == "Staff") {
                std::cout << "Cost             : \t" << dynamic_cast<Staff *>(p_weapon)->getMCost() << " Mana/hit"
                          << std::endl;
            } else if (Game::getTypeOfWeapon(p_weapon) == "Sword") {
                std::cout << "Durability       : \t" << dynamic_cast<Sword *>(p_weapon)->getMDurability() << std::endl;
            } else if (Game::getTypeOfWeapon(p_weapon) == "Dagger") {
                std::cout << "Durability       : \t" << dynamic_cast<Dagger *>(p_weapon)->getMDurability() << std::endl;
            } else if (Game::getTypeOfWeapon(p_weapon) == "Bow") {
                std::cout << "Arrows           : \t" << dynamic_cast<Bow *>(p_weapon)->getMArrows() << std::endl;
            }
            std::cout << "Bonuses          : \t" << std::endl;

            std::map<std::string, float> bonuses = p_weapon->getMBonus();
            if (bonuses.find("hp") != bonuses.end()) {
                if (bonuses.at("hp") > 0) {
                    std::cout << "\t +" << bonuses.at("hp") << " HP" << std::endl;
                } else if (bonuses.at("hp") < 0) {
                    std::cout << "\t " << bonuses.at("hp") << " HP" << std::endl;
                }
            }

            if (bonuses.find("intelligence") != bonuses.end()) {
                if (bonuses.at("intelligence") > 0) {
                    std::cout << "\t +" << bonuses.at("intelligence") << " Intelligence" << std::endl;
                } else if (bonuses.at("intelligence") < 0) {
                    std::cout << "\t " << bonuses.at("intelligence") << " Intelligence" << std::endl;
                }
            }
            if (bonuses.find("attack") != bonuses.end()) {
                if (bonuses.at("attack") > 0) {
                    std::cout << "\t +" << bonuses.at("attack") << " Attack" << std::endl;
                } else if (bonuses.at("attack") < 0) {
                    std::cout << "\t " << bonuses.at("attack") << " Attack" << std::endl;
                }
            }

            if (bonuses.find("agilite") != bonuses.end()) {
                if (bonuses.at("agilite") > 0) {
                    std::cout << "\t +" << bonuses.at("agilite") << " Agility" << std::endl;
                } else if (bonuses.at("agilite") < 0) {
                    std::cout << "\t " << bonuses.at("agilite") << " Agility" << std::endl;
                }
            }
        }
    }
    if (system("CLS")) system("clear");
}
