#include <iostream>
#include "WeaponParser.h"
#include "Sword.h"
#include "Dagger.h"
#include "Bow.h"
#include "Staff.h"

WeaponParser::WeaponParser() {

}

void WeaponParser::setMType(const std::string &mType) {
    m_type = mType;
}

void WeaponParser::setMNom(const std::string &mNom) {
    m_nom = mNom;
}

void WeaponParser::setMHp(const std::string &mHp) {
    m_hp = mHp;
}

void WeaponParser::setMCout(const std::string &mCout) {
    m_cout = mCout;
}

void WeaponParser::setMIntelligence(const std::string &mIntelligence) {
    m_intelligence = mIntelligence;
}

void WeaponParser::setMCritique(const std::string &mCritique) {
    m_critique = mCritique;
}

void WeaponParser::setMDegat(const std::string &mDegat) {
    m_degat = mDegat;
}

void WeaponParser::setMAttack(const std::string &mAttack) {
    m_attack = mAttack;
}

void WeaponParser::setMDurabilite(const std::string &mDurabilite) {
    m_durabilite = mDurabilite;
}

void WeaponParser::setMFleche(const std::string &mFleche) {
    m_fleche = mFleche;
}

void WeaponParser::setMAgilite(const std::string &mAgilite) {
    m_agilite = mAgilite;
}

void WeaponParser::toString() const {
    std::cout << this->m_nom << std::endl;
}

std::ostream &operator<<(std::ostream &os, const WeaponParser &parser) {
    os << "m_type: " << parser.m_type << " m_nom: " << parser.m_nom << " m_hp: " << parser.m_hp << " m_cout: "
       << parser.m_cout << " m_intelligence: " << parser.m_intelligence << " m_critique: " << parser.m_critique
       << " m_degat: " << parser.m_degat << " m_attack: " << parser.m_attack << " m_durabilite: " << parser.m_durabilite
       << " m_fleche: " << parser.m_fleche << " m_agilite: " << parser.m_agilite;
    return os;
}

std::map<std::string, float> WeaponParser::getBonuses() {
    std::map<std::string, float> bonuses;
    bonuses["hp"] = (this->m_hp.empty() ? 0.0f : std::stof(this->m_hp));
    bonuses["cout"] = (this->m_cout.empty() ? 0 : std::stoi(this->m_cout));
    bonuses["intelligence"] = (this->m_intelligence.empty() ? 0.0f : std::stof(this->m_intelligence));
    bonuses["critique"] = (this->m_critique.empty() ? 0.0f : std::stof(this->m_critique));
    bonuses["degat"] = (this->m_degat.empty() ? 0.0f : std::stof(this->m_degat));
    bonuses["attack"] = (this->m_attack.empty() ? 0.0f : std::stof(this->m_attack));
    bonuses["durabilite"] = (this->m_durabilite.empty() ? 0.0f : std::stof(this->m_durabilite));
    bonuses["fleche"] = (this->m_fleche.empty() ? 0.0f : std::stof(this->m_fleche));
    bonuses["agilite"] = (this->m_agilite.empty() ? 0.0f : std::stof(this->m_agilite));
    return bonuses;
}

Weapon *WeaponParser::getWeaponFromWeaponParser() {
    if (this->m_type == "Epee") {
        Sword *s = new Sword(this->m_nom, std::stof(this->m_degat), std::stof(this->m_critique),
                             std::stof(this->m_durabilite),
                             false);
        s->setMBonus(this->getBonuses());
        return s;

    } else if (this->m_type == "Dague") {
        Dagger *d = new Dagger(this->m_nom, std::stof(this->m_degat), std::stof(this->m_critique),
                               std::stof(this->m_durabilite),
                               false);
        d->setMBonus(this->getBonuses());
        return d;
    } else if (this->m_type == "Arc") {
        Bow *b = new Bow(this->m_nom, std::stof(this->m_degat), std::stof(this->m_critique),
                         this->getBonuses()["fleche"]);
        b->setMBonus(this->getBonuses());
        return b;
        //Case "Baton"
    } else {
        Staff *staff = new Staff(this->m_nom, std::stof(this->m_degat), std::stof(this->m_critique),
                                 this->getBonuses()["cout"]);
        staff->setMBonus(this->getBonuses());
        return staff;
    }
}
