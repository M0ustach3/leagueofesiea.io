#include <iostream>
#include "Warrior.h"
#include "Sword.h"


void Warrior::shout() {
    if (this->m_shoutsRemaining > 0) {
        std::cerr << "A shout is already active on this character" << std::endl;
    } else if (this->m_cooldownShout > 0) {
        std::cerr << "Wait a bit, only " << this->m_cooldownShout << " rounds to wait before roaring again..."
                  << std::endl;
    } else {
        int randTaunt = Game::getRandomInt(1, 2);
        this->setMShoutsRemaining(randTaunt);
        this->m_cooldownShout = 4;
        std::cout << this->m_name << " is roaring ! Everyone is now angry at him for " << randTaunt << " rounds"
                  << std::endl;
    }
}

/// Method used to repair a weapon
/// \param p_weaponToRepair the weapon to repair
void Warrior::repair(Weapon *p_weaponToRepair) {
    Melee *weapon;
    weapon = dynamic_cast<Melee *>(p_weaponToRepair);
    if (weapon) {
        //The weapon is broken
        if (weapon->getMDurability() <= 0) {
            weapon->setMDurability(1);
        } else {
            //Generate a random from 1 to BASE_DURABILITY
            int randRepair = Game::getRandomInt(1, weapon->getBaseDurability());
            weapon->setMDurability(weapon->getMDurability() + randRepair);

            std::cout << this->getMName() << " repaired " << p_weaponToRepair->getMName() << " of " << randRepair
                      << " use(s) " << std::endl;

            //If the durability gets higher than the base durability, reset it to the base durability
            if (weapon->getMDurability() > weapon->getBaseDurability()) {
                weapon->setMDurability(weapon->getBaseDurability());
            }
        }
    } else {
        std::cerr << p_weaponToRepair->getMName() << " is not repairable" << std::endl;
    }
}

Warrior::~Warrior() {
    //Don't destroy weapons here, do it in main
}

float Warrior::getMBlock() const {
    return m_block;
}

unsigned short Warrior::getMShoutsRemaining() const {
    return m_shoutsRemaining;
}

void Warrior::setMShoutsRemaining(unsigned short mShoutsRemaining) {
    m_shoutsRemaining = mShoutsRemaining;
}

Warrior::Warrior(const std::string &mName, int mLife, float mDodge, int mSpeed, int mAttack, int mDefense, int mAgility,
                 int mIntelligence, float mBlock) : Character(mName,
                                                              mLife,
                                                              mDodge,
                                                              mSpeed,
                                                              mAttack,
                                                              mDefense,
                                                              mAgility,
                                                              mIntelligence),
                                                    m_block(mBlock),
                                                    m_shoutsRemaining(
                                                            0),
                                                    m_cooldownShout(
                                                            0) {}

unsigned short Warrior::getMCooldownShout() const {
    return m_cooldownShout;
}

Weapon *Warrior::getMWeapon() const {
    return Character::getMWeapon();
}

void Warrior::setMWeapon(Weapon *mWeapon) {
    if (Game::getTypeOfWeapon(this->m_weapon) == "Sword") {
        this->m_dodge -= this->m_block;
    }
    if (Game::getTypeOfWeapon(mWeapon) == "Dagger") {
        Character::setMWeapon(mWeapon);
    } else if (Game::getTypeOfWeapon(mWeapon) == "Sword") {
        Character::setMWeapon(mWeapon);
        this->m_dodge += this->m_block;
    } else {
        std::cerr << "Incompatible weapon" << std::endl;
    }
}

void Warrior::decrementAllCooldowns() {

    if (this->m_cooldownShout > 0) {
        this->m_cooldownShout--;
    }
    if (this->m_shoutsRemaining > 0) {
        this->m_shoutsRemaining--;
    }
}

std::vector<std::string> Warrior::getAbilities() {
    std::vector<std::string> abilities;

    if (this->getMCooldownShout() > 0)
        abilities.push_back("Shout (Not available, " + std::to_string(this->getMCooldownShout()) + " rounds left)");
    else
        abilities.push_back("Shout");

    abilities.push_back("Repair weapon");

    return abilities;
}
