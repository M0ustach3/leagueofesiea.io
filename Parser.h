#ifndef LEAGUEOFESIEA_IO_PARSER_H
#define LEAGUEOFESIEA_IO_PARSER_H


#include "CharacterParser.h"
#include "WeaponParser.h"
#include "Character.h"
#include <vector>
#include <string>
#include <fstream>

/**
 * Class used to parse the data from the file
 */
class Parser {
public:
    /**
     * Class method used to parse characters from the input file
     * NOTE THAT THE FILE MUST BE IN THE SAME DIRECTORY AS THE GENERATED EXE FILE
     * @param p_inputFile the input file to parse
     * @return a vector of dummy class
     */
    static std::vector<CharacterParser> parseCharactersFromFile(std::string p_inputFile);

    /**
     * Class method used to parse weapons from the input file
     * @param p_inputFile the input file to parse
     * NOTE THAT THE FILE MUST BE IN THE SAME DIRECTORY AS THE GENERATED EXE FILE
     * @return
     */
    static std::vector<WeaponParser> parseWeaponsFromFile(std::string p_inputFile);

    /**
     * Method used to generate a character from the dummy class
     * @param p_characterParser the dummy object to be converted
     * @return a real Character to be used later on
     */
    static Character *getCharacterFromCharacterParser(CharacterParser *p_characterParser);
};


#endif //LEAGUEOFESIEA_IO_PARSER_H
