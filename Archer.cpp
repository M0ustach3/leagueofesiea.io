#include <iostream>
#include "Archer.h"
#include "exceptions/GameException.hpp"

/// Method used to aim with a bow
/// The new attack should NOT be calculated here
void Archer::aimOn() {
    if (this->m_weapon == nullptr || (this->m_weapon != nullptr && Game::getTypeOfWeapon(this->m_weapon) != "Bow")) {
        throw AimException(this->m_name + " has no bow");
    } else if (this->m_isAiming) {
        throw AimException(this->m_name + " is already aiming");
    } else {
        this->m_isAiming = true;
        std::cout << this->getMName() << " is focused for his next shot !" << std::endl;
    }
}

/// Method used to remove the aim with a bow
void Archer::aimOff() {
    this->m_isAiming = false;
}

/// Method used to give an antidote to a character
/// \param p_target the character to heal
void Archer::antidote(Character *p_target) {
    if (this->m_usages <= 0) {
        std::cerr << this->m_name << " cannot use the antidote, only 3 times by game :(" << std::endl;
    } else {
        if (p_target->getMIsPoisoned() == -1) {
            throw AntidoteException(p_target->getMName() + " is immune to poison");
        } else if (p_target->getMIsPoisoned() == 0) {
            throw AntidoteException(p_target->getMName() + " is not poisoned");
        } else {
            p_target->setMIsPoisoned(0);
            std::cout << p_target->getMName() << " is now healed from poison !" << std::endl;
            this->m_usages--;
        }
    }
}


Archer::~Archer() {
    //DO NOT DELETE HERE
}

bool Archer::isMIsAiming() const {
    return m_isAiming;
}

Archer::Archer(const std::string &mName, int mLife, float mDodge, int mSpeed, int mAttack, int mDefense, int mAgility,
               int mIntelligence) : Character(mName, mLife, mDodge, mSpeed, mAttack,
                                              mDefense, mAgility, mIntelligence), m_usages(3), m_isAiming(false) {
    this->m_isPoisoned = -1;
}

Weapon *Archer::getMWeapon() const {
    return this->m_weapon;
}

void Archer::setMWeapon(Weapon *mWeapon) {
    if (Game::getTypeOfWeapon(mWeapon) == "Dagger" || Game::getTypeOfWeapon(mWeapon) == "Bow") {
        Character::setMWeapon(mWeapon);
    } else {
        std::cerr << "Incompatible weapon" << std::endl;
    }
}

std::vector<std::string> Archer::getAbilities() {
    std::vector<std::string> abilities;

    if (this->isMIsAiming())
        abilities.push_back("Aim (Already aiming)");
    else
        abilities.push_back("Aim");

    if (this->m_usages != 0)
        abilities.push_back("Cure - " + std::to_string(this->getMUsages()) + " uses left");
    else
        abilities.push_back("Cure (No more use available)");

    return abilities;
}

unsigned char Archer::getMUsages() const {
    return m_usages;
}
