#ifndef LEAGUEOFESIEA_IO_WEAPONPARSER_H
#define LEAGUEOFESIEA_IO_WEAPONPARSER_H

#include <string>
#include <ostream>
#include <map>
#include "Weapon.h"

/**
 * Dummy class to use with the parser
 */
class WeaponParser {
public:
    WeaponParser();

    void setMType(const std::string &mType);

    void setMNom(const std::string &mNom);

    void setMHp(const std::string &mHp);

    void setMCout(const std::string &mCout);

    void setMIntelligence(const std::string &mIntelligence);

    void setMCritique(const std::string &mCritique);

    void setMDegat(const std::string &mDegat);

    void setMAttack(const std::string &mAttack);

    void setMDurabilite(const std::string &mDurabilite);

    void setMFleche(const std::string &mFleche);

    void setMAgilite(const std::string &mAgilite);

    void toString() const;

    std::map<std::string, float> getBonuses();

    Weapon *getWeaponFromWeaponParser();

    friend std::ostream &operator<<(std::ostream &os, const WeaponParser &parser);

private:
    std::string m_type, m_nom, m_hp, m_cout, m_intelligence, m_critique, m_degat, m_attack, m_durabilite, m_fleche, m_agilite;
};


#endif //LEAGUEOFESIEA_IO_WEAPONPARSER_H
