# OOP Project - RPG Game
## Pablo BONDIA-LUTTIAU and Théophile HAMELIN

--------------------------------

## Description
This project is an RPG, created for the course of Object-Oriented Programming.

_You can find a pretty version of this README file by viewing our GitLab : [here](https://gitlab.com/M0ustach3/leagueofesiea.io/blob/master/README.md)_ or
by using a Markdown interpreter (Ctrl+M on Atom) or a website doing it (you won't have the Visuals though : [here](https://markdownlivepreview.com/)

--------------------------------
## Features
- Hosted on GitHub with precise organization for the project (Tickets, Pull requests, multiple branches, explicit commit messages, etc.).
You can view a beautiful version of this README (and the others) on [GitHub](https://gitlab.com/M0ustach3/leagueofesiea.io)
- Modelized project with UML to ensure that we won't have any problems later on
- Automatic parsing of the provided files (weapons.jdc and characters.jdc)
- Custom exceptions creation and error handling via them (throw exceptions)
- Cool menu in console format, with a cursor, created from scratch without any library
- Colors used in the console
- All the classes are commented, with the syntax of Doxygen. The docs are generated under the ``docs/`` folder
- Automatic ranking of character and weapon stats based on the maximum and minimum values (colors
from red (bad), orange (meh), green (OP))
- Automatic filtering of the weapons corresponding to the class of the chosen character
- Least memory leaks we could (we hope none)

--------------------------------

## Badges
[![GPLv3 license](https://img.shields.io/static/v1?label=Licensed&message=GPLv3&color=red&style=for-the-badge&logo=appveyor)](https://choosealicense.com/licenses/gpl-3.0/)

--------------------------------

## Visuals

### UML Diagram

![UML Diagram](./Diagramme_UML.png "UML Diagram")


### Character selection menu

![Character selection menu](./selectCharacter.png "Character selection menu")

### Character info panel

![Character info panel](./characterDetails.png "Character info panel")

### Weapon info panel

![Weapon info panel](./weaponDetails.png "Weapon info panel")

### Attack with a weapon

![Attack with a weapon](./attackWithWeapon.png "Attack with a weapon")

![Attack with a bow](./afterArrow.png "Attack with a bow")

### Use an ability

![Use an ability](./useAbility.png "Use an ability")

--------------------------------

## **Important notes**
You __NEED__ to put the files weapons.jdc and characters.jdc in the __SAME__ directory as the output
exe. If you don't do this, the game won't work.


--------------------------------

## Prerequisites
You need to have a compatible console to correctly display the colors in the terminal.

This should be always the case as this project was tested in 3 different OSs and PCs :
- 1 PC running Windows 10 (compiled in x86 and x64 with Visual Studio 2019 Community)
- 1 PC running MacOS Catalina 10.15.2 (Compiled with CLion)
- 1 PC running Linux Mint (Compiled with CLion)

We didn't had any display problems when compiling on those 3 PCs.

--------------------------------

## Installation
Just build the project using the Local Debugger of Visual Studio, or by double clicking on the
produced .exe file.


--------------------------------

## Usage
For the usage, just launch our program, everything is explained.

But if you need it, there it is :
- z/s to move up/down
- d to confirm
- q to go back on certain menus

--------------------------------

## Support
Meh, no support needed, there are no bugs in our code (⌒_⌒;)

--------------------------------

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

--------------------------------

## Authors
Made with :heart: by Pablo Bondia-Luttiau and Théophile Hamelin

--------------------------------

## License
This project is licensed under [GPLv3](https://choosealicense.com/licenses/gpl-3.0/).

--------------------------------

## Project Status
This project will no be maintained after 17/01/2020 (Project deadline).
# (╯°□°）╯︵ ┻━┻
