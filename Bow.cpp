#include "Bow.h"

Bow::Bow(const std::string &mName, float mDamage, float mCritical, int mArrows) : Ranged(mName, mDamage, mCritical),
                                                                                  m_arrows(mArrows) {
}

Bow::~Bow() {

}

int Bow::getMArrows() const {
    return m_arrows;
}

void Bow::setMArrows(int mArrows) {
    m_arrows = mArrows;
}
