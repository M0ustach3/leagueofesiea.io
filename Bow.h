#ifndef BOW_H
#define BOW_H

#include "Ranged.h"

/**
 * Class Bow, representing a bow
 */
class Bow : public Ranged {
private:
    /**
     * Field containing the number of arrows
     */
    int m_arrows;
public:
    /**
     * Constructor of a bow
     * @param mName the name
     * @param mDamage the damages
     * @param mCritical the critical prob
     * @param mArrows the arrows
     */
    Bow(const std::string &mName, float mDamage, float mCritical, int mArrows);

    /**
     * The destructor of the bow
     */
    virtual ~Bow();

    /**
     * Method used to get the number of arrows of the bow
     * @return the number of arrows
     */
    int getMArrows() const;

    /**
     * Method used to set the number of arrows
     * @param mArrows the new number of arrows
     */
    void setMArrows(int mArrows);
};

#endif
