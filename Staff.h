#ifndef STAFF_H
#define STAFF_H

#include "Ranged.h"

/**
 * Class representing a staff
 */
class Staff : public Ranged {
private:
    /**
     * The mana cost per use
     */
    int m_cost;
public:
    /**
     * Constructor of a staff
     * @param mName the name
     * @param mDamage the damage
     * @param mCritical the critical probability
     * @param mCost the mana cost per use
     */
    Staff(const std::string &mName, float mDamage, float mCritical, int mCost);

    /**
     * Destructor of staff
     */
    virtual ~Staff();

    /**
     * Getter for the mana cost
     * @return the cost of mana per usage
     */
    int getMCost() const;
};

#endif
