#include "Player.h"

Player::Player(const std::string &mName) : m_name(mName), m_currentCharacter(0) {

}

const std::string &Player::getMName() const {
    return m_name;
}

const std::vector<Character *> &Player::getMCharacters() const {
    return m_characters;
}

void Player::setMCharacters(const std::vector<Character *> &mCharacters) {
    m_characters = mCharacters;
}

Player::~Player() {

    for (Character *character : this->m_characters) {
        delete character;
    }

    this->m_characters.clear();
}

