#ifndef RANGED_H
#define RANGED_H

#include "Weapon.h"

/**
 * Class used to describe a ranged weapon
 */
class Ranged : public Weapon {
public:
    /**
     * Constructor for the ranged weapon
     * @param mName the name of the ranged weapon
     * @param mDamage the damage of the ranged weapon
     * @param mCritical the critical probability
     */
    Ranged(const std::string &mName, float mDamage, float mCritical);
};

#endif
