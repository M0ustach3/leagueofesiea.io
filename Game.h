#ifndef LEAGUEOFESIEA_IO_GAME_H
#define LEAGUEOFESIEA_IO_GAME_H


#include "Player.h"
#include "Weapon.h"

/**
 * Class used to describe the behaviour of the game
 */
class Game {
private:
    /**
     * The speed of the first and the second player
     */
    int m_speedCharacterJ1, m_speedCharacterJ2;
    /**
     * The players, and the current player
     */
    Player *m_player1, *m_player2, *m_currentPlayer;

public:
    /**
     * Constructor of a game
     * @param mPlayer1 The first player
     * @param mPlayer2 The second player
     */
    Game(Player *mPlayer1, Player *mPlayer2);

    /**
     * Getter of player 1
     * @return the first player
     */
    Player *getMPlayer1() const;

    /**
     * Setter of player 1
     * @param mPlayer1 the new Player 1
     */
    void setMPlayer1(Player *mPlayer1);

    /**
     * Getter of player 2
     * @return the second player
     */
    Player *getMPlayer2() const;

    /**
     * Setter of player 2
     * @param mPlayer1 the new Player 2
     */
    void setMPlayer2(Player *mPlayer2);

    /**
     * Class method to get a random float
     * @param p_min the minimal value (included)
     * @param p_max the maximal value (excluded)
     * @return a value between the limits
     */
    static float getRandom(float p_min, float p_max);

    /**
        * Class method to get a random int
        * @param p_min the minimal value (included)
        * @param p_max the maximal value (included)
        * @return a value between the limits
        */
    static int getRandomInt(int p_min, int p_max);

    /**
     * Class method used to get the type of a character, in a string format
     * @param c the character to test
     * @return a string representing the character, "Unknown" if the type is unknown
     */
    static std::string getTypeOfCharacter(Character *c);

    /**
     * Class method used to get the type of a weapon, in a string format
     * @param w the weapon to test
     * @return a string representing the weapon, "Unknown" if the type is unknown
     */
    static std::string getTypeOfWeapon(Weapon *w);

    /**
     * Class method used to test if any character is shouting in a specific deck
     * @param targetDeck the target deck to test
     * @return The first occurence of a shouting character, nullptr if no characters are shouting
     */
    static Character *isAnyCharacterShouting(const std::vector<Character *> &targetDeck);

    /**
     * Class method used to test if all players vanished in a target deck
     * @param targetDeck the target deck to test
     * @return true if all players vanished, false otherwise
     */
    static bool areAllPlayersVanished(const std::vector<Character *> &targetDeck);

    /**
     * Method used to go to the next round
     */
    void nextRound();

    /**
     * Destructor of game
     */
    virtual ~Game();

    /**
     * Method used to get the current player
     * @return the current player
     */
    Player *getMCurrentPlayer() const;

    /**
     * Method used to get the enemy of the current player
     * @return the enemy of the current player
     */
    Player *getEnnemyOfCurrentPlayer();

    /**
     * Class method used to test if a character is compatible with a specific weapon
     * @param p_character the character to test
     * @param p_weapon the weapon to test
     * @return true if the character is compatible, false otherwise
     */
    static bool isCharacterCompatibleWithWeapon(Character *p_character, Weapon *p_weapon);

    /**
     * Method used to initiate the first player of the game
     */
    void setFirstPlayer();

    /**
     * Method used to test if a player is out
     * @param p_player the player to test
     * @return true if the player is out of the game, false otherwise
     */
    bool isPlayerOut(Player *p_player);
};


#endif //LEAGUEOFESIEA_IO_GAME_H
