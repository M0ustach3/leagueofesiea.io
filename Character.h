#include <string>
#include <vector>
#include "Game.h"
#include "Player.h"
#include "Weapon.h"


#ifndef CHARACTER_H
#define CHARACTER_H

/**
 * Class representing a character
 */
class Character {
protected:
    /**
     * Represents the owner of the character
     */
    Player *m_owner;
    /**
     * Represents the name of the character
     */
    std::string m_name;
    /**
     * Represents the life of the character
     */
    int m_life;
    /**
     * Represents the base life of a character
     */
    int BASE_LIFE;
public:
    int getBaseLife() const;

protected:
    /**
     * Represents the dodge probability of the character
     */
    float m_dodge;
    /**
     * Represents the speed of the character
     */
    int m_speed;
    /**
     * Represents the attack of the character
     */
    int m_attack;
    /**
     * Represents the defense of the character
     */
    int m_defense;
    /**
     * Represents the agility of the character
     */
    int m_agility;
    /**
     * Represents the intelligence of the character
     */
    int m_intelligence;
    /**
     * Represents if the character is poisoned (more than 0), if it isn't (0) or
     * if it is immuned to it (-1)
     */
    int m_isPoisoned;
    /**
     * Represents the current weapon of the character
     */
    Weapon *m_weapon;

public:
    /**
     * Constructor of a character
     * @param mName the name
     * @param mLife the life
     * @param mDodge the dodge probability
     * @param mSpeed the speed
     * @param mAttack the attack
     * @param mDefense the defense
     * @param mAgility the agility
     * @param mIntelligence the intelligence
     */
    Character(const std::string &mName, int mLife, float mDodge, int mSpeed, int mAttack, int mDefense, int mAgility,
              int mIntelligence);

    /**
     * Method used to get the weapon of the character
     * @return a ptr to the weapon of the character
     */
    virtual Weapon *getMWeapon() const;

    /**
     * Method used to set the weapon of the character
     * @param mWeapon
     */
    virtual void setMWeapon(Weapon *mWeapon);

    /**
     * Method used to get the name of the character
     * @return the name of the character
     */
    const std::string &getMName() const;

    /**
     * Method used to set the name of the character
     * @param mName the new name of the character
     */
    void setMName(const std::string &mName);

    /**
     * Method used to get the life of the character
     * @return the life of the character
     */
    int getMLife() const;

    /**
     * Method used to set the life of the character
     * @param mLife the new life of the character
     */
    void setMLife(int mLife);

    /**
     * Method used to get the dodge probability of the character
     * @return the dodge probability
     */
    float getMDodge() const;

    /**
     * Method used to get the speed of the character
     * @return the speed of the character
     */
    int getMSpeed() const;

    /**
     * Method used to get the attack of the character
     * @return the attack of the character
     */
    int getMAttack() const;

    /**
     * Method used to get the defense of the character
     * @return the defense of the character
     */
    int getMDefense() const;

    /**
     * Method used to get the agility of the character
     * @return the agility of the character
     */
    int getMAgility() const;

    /**
     * Method used to get the intelligence of the character
     * @return the intelligence of the character
     */
    int getMIntelligence() const;

    /**
     * Method used to get the poisoned value of the character
     * @return -1 if the character is immune to poison, 0 if is isn't poisoned
     */
    int getMIsPoisoned() const;

    /**
     * Method used to set if the character is poisoned
     * @param mIsPoisoned the new value of the poison
     */
    void setMIsPoisoned(int mIsPoisoned);

    /**
     * Method used to get the abilities of the character to be displayed in the menu
     * @return a vector of strings representing the different abilities of the character
     */
    virtual std::vector<std::string> getAbilities();

    /**
     * Method used to get the owner of the character
     * @return the owner of the character
     */
    Player *getMOwner() const;

    /**
     * Method used to set the owner of the character
     * @param mOwner the new owner of the character
     */
    void setMOwner(Player *mOwner);

    /**
     * Method used to punch another character
     * @param p_target the target to punch
     */
    void punch(Character *p_target);

    /**
     * Method used to attack with the weapon of the character
     * @param p_target the target to attack
     */
    void attackWithWeapon(Character *p_target);

    /**
     * Method used to decrement all cooldowns of the character
     */
    virtual void decrementAllCooldowns();

    /**
     * Destructor of Character
     */
    virtual ~Character();

};

#endif
