var searchData =
    [
        ['isanycharactershouting_56', ['isAnyCharacterShouting', ['../class_game.html#a0c221289e2c218191a7f2ce834a44741', 1, 'Game']]],
        ['ischaractercompatiblewithweapon_57', ['isCharacterCompatibleWithWeapon', ['../class_game.html#acc921f7c910737b9ac633462a16e86e0', 1, 'Game']]],
        ['ismisaiming_58', ['isMIsAiming', ['../class_archer.html#a364bdb4b3945741e1fe0b79db9678e28', 1, 'Archer']]],
        ['ismisenchanted_59', ['isMIsEnchanted', ['../class_melee.html#a35a946f1c3879bad626f94ef915b810a', 1, 'Melee']]],
        ['ismisinvisible_60', ['isMIsInvisible', ['../class_rogue.html#a6aa0709b56ca57775aa15d018156622f', 1, 'Rogue']]],
        ['isplayerout_61', ['isPlayerOut', ['../class_game.html#a04d9806a8d66cfb9fb1812c9fe92fa7c', 1, 'Game']]]
    ];
