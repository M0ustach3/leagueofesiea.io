var searchData =
    [
        ['m_5fagility_62', ['m_agility', ['../class_character.html#a6f93eadd2af6179f703af95b8945c392', 1, 'Character']]],
        ['m_5fattack_63', ['m_attack', ['../class_character.html#a0cd77c1edc35015c34f7ee1f42a5d8c1', 1, 'Character']]],
        ['m_5fbonus_64', ['m_bonus', ['../class_weapon.html#a57066c71b59523fc5d7b84efc516f3b0', 1, 'Weapon']]],
        ['m_5fcritical_65', ['m_critical', ['../class_weapon.html#a4fbaa1fbb6b74b1fccd0ad8b17c3cbc9', 1, 'Weapon']]],
        ['m_5fdamage_66', ['m_damage', ['../class_weapon.html#ab3b0436ea7a335a0338c016ecf527379', 1, 'Weapon']]],
        ['m_5fdefense_67', ['m_defense', ['../class_character.html#a3e6aea7df25e0ec53c72cacbb0044cba', 1, 'Character']]],
        ['m_5fdodge_68', ['m_dodge', ['../class_character.html#af84d3452152196c58fd7be914c13e5be', 1, 'Character']]],
        ['m_5fdurability_69', ['m_durability', ['../class_melee.html#ab080c12d6604f7f311295c5e395d994f', 1, 'Melee']]],
        ['m_5fintelligence_70', ['m_intelligence', ['../class_character.html#a028e6b2f25e36716c548b41e96c33497', 1, 'Character']]],
        ['m_5fisenchanted_71', ['m_isEnchanted', ['../class_melee.html#af78b71a55f5f67aa28b3d0760085ffd2', 1, 'Melee']]],
        ['m_5fispoisoned_72', ['m_isPoisoned', ['../class_character.html#ad7cd5b29fed876e2ef0e2186dc111b93', 1, 'Character']]],
        ['m_5flife_73', ['m_life', ['../class_character.html#a283fdcc912919a31142bd8af74a16689', 1, 'Character']]],
        ['m_5fname_74', ['m_name', ['../class_character.html#ab4daf763291bcf1aaa9cd7caaab9c6fe', 1, 'Character::m_name()'], ['../class_weapon.html#a1e8553597d2d3c13e9c352826be0f131', 1, 'Weapon::m_name()']]],
        ['m_5fowner_75', ['m_owner', ['../class_character.html#a46e961ec564569546965a871db394f7e', 1, 'Character']]],
        ['m_5fspeed_76', ['m_speed', ['../class_character.html#ab0b8ba10b25da5ca7bcea0abeb4ed0d0', 1, 'Character']]],
        ['m_5fweapon_77', ['m_weapon', ['../class_character.html#adfa4b9f680db1919c1b5dbec70e39c17', 1, 'Character']]],
        ['melee_78', ['Melee', ['../class_melee.html', 1, 'Melee'], ['../class_melee.html#ab6c08bcf063e7e83edfea9d51a11f88f', 1, 'Melee::Melee()']]],
        ['menu_79', ['Menu', ['../class_menu.html', 1, 'Menu'], ['../class_menu.html#ad466dd83355124a6ed958430450bfe94', 1, 'Menu::Menu()']]]
    ];
