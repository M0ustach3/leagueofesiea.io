var searchData =
    [
        ['setfirstplayer_234', ['setFirstPlayer', ['../class_game.html#a2364c04804f3c478626e930c2dfc803a', 1, 'Game']]],
        ['setmarrows_235', ['setMArrows', ['../class_bow.html#adfb772590b8460c405d7a393aa5cfa49', 1, 'Bow']]],
        ['setmbonus_236', ['setMBonus', ['../class_weapon.html#ade8c4df121f22dcfe2bd7f605e3875bf', 1, 'Weapon']]],
        ['setmcharacters_237', ['setMCharacters', ['../class_player.html#ab1bed4ba3de3c890bf234f4df09cc442', 1, 'Player']]],
        ['setmdamage_238', ['setMDamage', ['../class_weapon.html#abeba8845fcdda5e3342c77890a8d77e0', 1, 'Weapon']]],
        ['setmdurability_239', ['setMDurability', ['../class_melee.html#a58dbb8976bac417d29ba0a9fdb3b5684', 1, 'Melee']]],
        ['setmisenchanted_240', ['setMIsEnchanted', ['../class_melee.html#a8356b77c9c37d2bde75a2ee95e360518', 1, 'Melee']]],
        ['setmisinvisible_241', ['setMIsInvisible', ['../class_rogue.html#af0d6e29cda40fe7c645ccdbf9a129fc0', 1, 'Rogue']]],
        ['setmispoisoned_242', ['setMIsPoisoned', ['../class_character.html#a92ff17733cca4a6f4d950483eb742559', 1, 'Character']]],
        ['setmlife_243', ['setMLife', ['../class_character.html#a479e51816cf337853a2073c17e21cbf1', 1, 'Character']]],
        ['setmmana_244', ['setMMana', ['../class_sorcerer.html#a44a2f58dd0d18b1a46b8d4a051440c7b', 1, 'Sorcerer']]],
        ['setmname_245', ['setMName', ['../class_character.html#a09f46d131086f17363eb377e3bdb2f06', 1, 'Character']]],
        ['setmowner_246', ['setMOwner', ['../class_character.html#a67401ba4071811cb2affa1494e2c1717', 1, 'Character']]],
        ['setmplayer1_247', ['setMPlayer1', ['../class_game.html#a874dc3e7a1cb67661f6e7a5fd7dd2ed9', 1, 'Game']]],
        ['setmplayer2_248', ['setMPlayer2', ['../class_game.html#af7a4756c27469e826d8dd2e578f70fd6', 1, 'Game']]],
        ['setmshoutsremaining_249', ['setMShoutsRemaining', ['../class_warrior.html#a2d294e7573dd4cbfd07618bae92d5a9d', 1, 'Warrior']]],
        ['setmweapon_250', ['setMWeapon', ['../class_archer.html#a87a65e76a7f78616fe76017af4fd229d', 1, 'Archer::setMWeapon()'], ['../class_character.html#af8320d51d91fec9118adbd4689ea992e', 1, 'Character::setMWeapon()'], ['../class_rogue.html#aac6381d3bde6b0c56cf4e134a7879fa7', 1, 'Rogue::setMWeapon()'], ['../class_sorcerer.html#a72ac3fb45571c09d57b7531d79dbf6aa', 1, 'Sorcerer::setMWeapon()'], ['../class_warrior.html#ae2945845244db1b49700a0ee5d81808f', 1, 'Warrior::setMWeapon()']]],
        ['shout_251', ['shout', ['../class_warrior.html#a6354a1061474b9813af6b072ab60d6fd', 1, 'Warrior']]],
        ['sorcerer_252', ['Sorcerer', ['../class_sorcerer.html#a55ff7bb33154cd47879c4b57192f4fad', 1, 'Sorcerer']]],
        ['staff_253', ['Staff', ['../class_staff.html#aacedea6f0c4439b462bc56ceccb80262', 1, 'Staff']]],
        ['sword_254', ['Sword', ['../class_sword.html#a950ba4840f7da750b9d8e7cf94e4fd0e', 1, 'Sword']]]
    ];
