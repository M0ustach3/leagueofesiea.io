#include <iostream>
#include "Game.h"
#include "Menu.h"
#include "Parser.h"
#include "exceptions/ParserException.hpp"


int main() {
    Menu menu;
    std::vector<CharacterParser> availableCharactersJ1, availableCharactersJ2;
    std::vector<WeaponParser> availableWeaponsJ1, availableWeaponsJ2;

    try {
        availableCharactersJ1 = Parser::parseCharactersFromFile("characters.jdc");
        availableCharactersJ2 = Parser::parseCharactersFromFile("characters.jdc");
        availableWeaponsJ1 = Parser::parseWeaponsFromFile("weapons.jdc");
        availableWeaponsJ2 = Parser::parseWeaponsFromFile("weapons.jdc");
    }
    catch (ParserException & parserException) {
        std::cerr << "Couldn't open '" << parserException.what()
            << "' for reading. Did you put in in the same directory as the exe file?" << std::endl;
        return -1;
    }


    Game* game = new Game(nullptr, nullptr);
    menu.choosePlayers(game);

    menu.chooseCharacter(game->getMPlayer1(), &availableCharactersJ1, &availableWeaponsJ1);
    menu.chooseCharacter(game->getMPlayer2(), &availableCharactersJ2, &availableWeaponsJ2);

    game->setFirstPlayer();


    bool wantsContinue = true;
    char input;

    while (wantsContinue) {
        while (!game->isPlayerOut(game->getMPlayer1()) && !game->isPlayerOut(game->getMPlayer2())) {
            menu.printCharacterSelectionMenuRound(game->getMCurrentPlayer(), game);
            game->nextRound();
        }

        if (game->isPlayerOut(game->getMPlayer1())) {
            std::cout << game->getMPlayer1()->getMName() << " GAME OVER" << std::endl;
            std::cout << game->getMPlayer2()->getMName() << " has won the game !" << std::endl;
        }
        else {
            std::cout << game->getMPlayer2()->getMName() << " GAME OVER" << std::endl;
            std::cout << game->getMPlayer1()->getMName() << " has won the game !" << std::endl;
        }
        do {
            std::cout << "Do you want to restart ? Y/N" << std::endl;
            std::cin >> input;
        } while (input != 'n' && input != 'N' && input != 'y' && input != 'Y');
        if (input == 'n' || input == 'N') {
            wantsContinue = false;
            std::cin.ignore();
            break;
        }
        else {
            availableWeaponsJ1.clear();
            availableWeaponsJ2.clear();
            availableCharactersJ1.clear();
            availableCharactersJ2.clear();

            for (const CharacterParser& parser : Parser::parseCharactersFromFile("characters.jdc")) {
                availableCharactersJ1.push_back(parser);
                availableCharactersJ2.push_back(parser);
            }

            for (const WeaponParser& parser : Parser::parseWeaponsFromFile("weapons.jdc")) {
                availableWeaponsJ1.push_back(parser);
                availableWeaponsJ2.push_back(parser);
            }

            menu.chooseCharacter(game->getMPlayer1(), &availableCharactersJ1, &availableWeaponsJ1);
            menu.chooseCharacter(game->getMPlayer2(), &availableCharactersJ2, &availableWeaponsJ2);

            game->setFirstPlayer();
        }
    }

    availableWeaponsJ1.clear();
    availableWeaponsJ2.clear();
    availableCharactersJ1.clear();
    availableCharactersJ2.clear();

    delete game;
    return 0;
}
