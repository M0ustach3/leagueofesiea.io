#include "Weapon.h"


Weapon::Weapon(const std::string &mName, float mDamage, float mCritical)
        : m_name(mName), m_damage(mDamage), m_critical(mCritical) {
}

const std::string &Weapon::getMName() const {
    return m_name;
}

float Weapon::getMDamage() const {
    return m_damage;
}

void Weapon::setMDamage(float mDamage) {
    m_damage = mDamage;
}

float Weapon::getMCritical() const {
    return m_critical;
}

const std::map<std::string, float> &Weapon::getMBonus() const {
    return m_bonus;
}

void Weapon::setMBonus(const std::map<std::string, float> &mBonus) {
    m_bonus = mBonus;
}

Weapon::~Weapon() = default;
