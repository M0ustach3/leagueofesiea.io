#include "Sword.h"


Sword::Sword(const std::string &mName, float mDamage, float mCritical, int mDurability, bool mIsEnchanted) : Melee(
        mName, mDamage, mCritical, mDurability, mIsEnchanted) {}

Sword::~Sword() {

}
