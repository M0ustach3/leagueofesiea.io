#include "Staff.h"


Staff::Staff(const std::string &mName, float mDamage, float mCritical, int mCost) : Ranged(mName, mDamage, mCritical),
                                                                                    m_cost(mCost) {}

Staff::~Staff() {

}

int Staff::getMCost() const {
    return m_cost;
}

