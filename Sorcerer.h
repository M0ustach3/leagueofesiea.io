#ifndef SORCERER_H
#define SORCERER_H

#include <string>
#include <vector>

#include "Character.h"
#include "Staff.h"
#include "Sword.h"

/**
 * Class representing a sorcerer
 */
class Sorcerer : public Character {
private:
    /**
     * The quantity of mana of the sorcerer
     */
    unsigned int m_mana;
    /**
     * The base mana of the sorcerer
     */
    unsigned int BASE_MANA;
    /**
     * The cooldown for the enchant
     */
    unsigned int m_cooldownEnchant;

public:

    /**
     * Constructor of the sorcerer
     * @param mName the name
     * @param mLife the life
     * @param mDodge the dodge probability
     * @param mSpeed the speed
     * @param mAttack the attack
     * @param mDefense the defense
     * @param mAgility the agility
     * @param mIntelligence the intelligence
     */
    Sorcerer(const std::string &mName, int mLife, float mDodge, int mSpeed, int mAttack, int mDefense, int mAgility,
             int mIntelligence);

    /**
     * Method used to enchant a weapon
     * @param p_target the weapon to enchant
     */
    void enchant(Weapon *p_target);

    /**
     * Method used to heal a character
     * @param p_target the character to heal
     */
    void heal(Character *p_target);

    /**
     * Method used to give back mana to the sorcerer
     */
    void regenerate();

    /**
     * Destructor for the sorcerer
     */
    virtual ~Sorcerer();

    /**
     * Getter for the mana
     * @return the quantity of mana
     */
    unsigned int getMMana() const;

    /**
     * The setter of the mana
     * @param mMana the mana value
     */
    void setMMana(unsigned int mMana);

    /**
     * Getter for the enchant cooldown
     * @return the enchant cooldown value
     */
    unsigned int getMCooldownEnchant() const;

    /**
     * Getter for the weapon
     * @return the weapon of the sorcerer
     */
    Weapon *getMWeapon() const override;

    /**
     * Setter for the weapon
     * @param mWeapon the new weapon
     */
    void setMWeapon(Weapon *mWeapon) override;

    /**
     * Method used to decrement all the cooldowns
     */
    void decrementAllCooldowns() override;

    /**
     * Method used to get all the abilities of the sorcerer
     * @return a vector of string representing the abilities
     */
    std::vector<std::string> getAbilities() override;

    unsigned int getBaseMana() const;

};

#endif
