#include <iostream>
#include "Character.h"
#include "Warrior.h"
#include "Archer.h"
#include "Rogue.h"
#include "Sword.h"
#include "Staff.h"
#include "Sorcerer.h"
#include "exceptions/GameException.hpp"

namespace colors {
    extern std::string red;
    extern std::string orange;
    extern std::string green;
    extern std::string clear;
    extern std::string magenta;
    extern std::string blue;
}

/// Allows a character to attack with his weapon
/// \param p_target The character to attack
void Character::attackWithWeapon(Character *p_target) {
    if (system("CLS")) system("clear");

    //Check if the character can attack (mana cost, arrows left)
    if (Game::getTypeOfWeapon(this->getMWeapon()) == "Staff") {
        //Check if the sorcerer has enough mana to use his staff
        if (dynamic_cast<Staff *>(this->getMWeapon())->getMCost() > dynamic_cast<Sorcerer *>(this)->getMMana()) {
            //Cost is too high
            throw ManaException("Not enough mana");
        }
    } else if (Game::getTypeOfWeapon(this->getMWeapon()) == "Bow") {
        //Check if the Archer or the Rogue has at least one arrow to attack
        if (dynamic_cast<Bow *>(this->getMWeapon())->getMArrows() <= 0) {
            throw ArrowsException();
        }
    }

    //The target is an invisible rogue
    //Check if the target is a rogue
    if (Game::getTypeOfCharacter(p_target) == "Rogue") {
        if (dynamic_cast<Rogue *>(p_target)->isMIsInvisible()) {
            if (Game::areAllPlayersVanished(p_target->getMOwner()->getMCharacters())) {
                std::cout << "Oh too bad " << colors::blue << p_target->getMName() << colors::clear
                          << ", everyone has already vanished, you'll be targeted anyway ¯\\_(ツ)_/¯" << std::endl;
            } else {
                throw VanishException(p_target->m_name + " has vanished");
            }
        }
    }
    //If the target has an owner (just a simple check)
    if (p_target->getMOwner() != nullptr) {
        //Warrior's taunt check
        std::vector<Character *> charactersOfTheEnnemyPlayer = p_target->getMOwner()->getMCharacters();
        if (Game::isAnyCharacterShouting(charactersOfTheEnnemyPlayer) != nullptr) {
            p_target = Game::isAnyCharacterShouting(charactersOfTheEnnemyPlayer);
            std::cout << colors::blue << p_target->getMName() << colors::clear << " is shouting, " << this->getMName()
                      << " is forced to attack him !" << std::endl;
        }

        //Dodge check
        float randomDodge = Game::getRandom(0.f, 1.f);
        float dodgeProb = p_target->getMDodge();
        //The warrior with a sword has an extra chance to dodge
        if (Game::getTypeOfCharacter(p_target) == "Warrior") {
            dodgeProb += dynamic_cast<Warrior *>(p_target)->getMBlock();
        }

        //The attack is a success
        if (dodgeProb < randomDodge) {
            int hpCurrent = p_target->getMLife();
            float coeff = Game::getRandom(0.85f, 1.f);
            int damages = 0;

            //Enchantment check
            bool enchant = false;
            if (Game::getTypeOfWeapon(this->getMWeapon()) == "Dagger") {
                if (dynamic_cast<Dagger *>(this->getMWeapon())->isMIsEnchanted()) {
                    enchant = true;
                    std::cout << colors::blue << this->getMName() << colors::clear
                              << "'s weapon is enchanted, it will do more damages !"
                              << std::endl;
                    this->getMWeapon()->setMDamage(this->getMWeapon()->getMDamage() * 1.33);
                }
            } else if (Game::getTypeOfWeapon(this->getMWeapon()) == "Sword") {
                if (dynamic_cast<Sword *>(this->getMWeapon())->isMIsEnchanted()) {
                    enchant = true;
                    std::cout << colors::blue << this->getMName() << colors::clear
                              << "'s weapon is enchanted, it will do more damages !"
                              << std::endl;
                    this->getMWeapon()->setMDamage(this->getMWeapon()->getMDamage() * 1.33);
                }
            }

            //Computing damages according to attacker's class
            if (Game::getTypeOfCharacter(this) == "Archer") {
                //IsAiming check and Bow check
                if (dynamic_cast<Archer *>(this)->isMIsAiming() &&
                    Game::getTypeOfWeapon(this->getMWeapon()) == "Bow") {
                    std::cout << "Headshot ! ―(T_T)→" << std::endl;
                    damages = 9 * (((this->getMAgility() * 1.33) + this->getMWeapon()->getMDamage()) /
                                   p_target->getMDefense()) * coeff;
                    dynamic_cast<Archer *>(this)->aimOff();
                } else {
                    damages = 9 *
                              ((this->getMAgility() + this->getMWeapon()->getMDamage()) / p_target->getMDefense()) *
                              coeff;
                }
            } else if (Game::getTypeOfCharacter(this) == "Warrior") {
                damages = 7 * ((this->getMAttack() + this->getMWeapon()->getMDamage()) / p_target->getMDefense()) *
                          coeff;
            } else if (Game::getTypeOfCharacter(this) == "Rogue") {
                damages = 7 * ((this->getMAgility() + this->getMWeapon()->getMDamage()) / p_target->getMDefense()) *
                          coeff;

                //Poison check with immunity check and dagger check
                if (p_target->getMIsPoisoned() != -1 && Game::getTypeOfWeapon(this->getMWeapon()) == "Dagger") {
                    float randomPoison = Game::getRandom(0.f, 1.f);
                    float poisonProb = dynamic_cast<Rogue *>(this)->getMPoison();

                    //Poison success
                    if (poisonProb > randomPoison) {
                        p_target->setMIsPoisoned(1);
                        std::cout << colors::blue << p_target->getMName() << colors::clear << " is poisoned !"
                                  << std::endl;
                    }
                } else {
                    std::cout << colors::blue << p_target->getMName() << colors::clear << " can't be poisoned !"
                              << std::endl;
                }

            } else if (Game::getTypeOfCharacter(this) == "Sorcerer") {
                damages = 5 * ((this->getMIntelligence() + this->getMWeapon()->getMDamage()) /
                               p_target->getMDefense()) * coeff;
            }

            //Critical strike check
            float randomCrit = Game::getRandom(0.f, 1.f);
            float critProb = this->getMWeapon()->getMCritical();

            //Critical strike !
            if (critProb > randomCrit) {
                damages = damages * 2;
                std::cout << "Critical strike !" << std::endl;
            }

            //Application of damages
            std::cout << colors::blue << this->getMName() << colors::clear << " inflicts " << colors::red << damages
                      << colors::clear
                      << " damage points to " << colors::blue
                      << p_target->getMName() << colors::clear
                      << "!" << std::endl;
            int hpLeft = hpCurrent - damages;
            if (hpLeft <= 0) {
                std::cout << colors::blue << p_target->getMName() << colors::clear << " is dead !" << std::endl;
                p_target->setMLife(0);
                if (dynamic_cast<Warrior *>(p_target) != nullptr) {
                    dynamic_cast<Warrior *>(p_target)->setMShoutsRemaining(0);
                }
                p_target->setMName(colors::red + p_target->getMName() + " - DEAD X_X" + colors::clear);
            } else {
                p_target->setMLife(hpLeft);
            }

            //Enchantment check, to reset damages
            if (enchant) {
                enchant = false;
                this->getMWeapon()->setMDamage(this->getMWeapon()->getMDamage() / 1.33);
                std::cout << this->getMWeapon()->getMName() << " is not enchanted anymore !" << std::endl;
                Melee *theWeapon;
                theWeapon = dynamic_cast<Melee *>(this->getMWeapon());
                if (theWeapon) {
                    theWeapon->setMIsEnchanted(false);
                }
                delete theWeapon;
            }

            //Weapon durability
            if (Game::getTypeOfWeapon(this->getMWeapon()) == "Sword") {
                int currentDurability = dynamic_cast<Sword *>(this->getMWeapon())->getMDurability();
                int randomWear = Game::getRandomInt(2, 5);
                int durabilityLeft = currentDurability - randomWear;

                if (durabilityLeft <= 0) {
                    dynamic_cast<Sword *>(this->getMWeapon())->setMDurability(0);
                    this->getMWeapon()->setMDamage(this->getMWeapon()->getMDamage() / 10);
                    std::cout << colors::blue << this->getMName() << colors::clear
                              << "'s weapon is broken ! His damages value is divided by 10 !"
                              << std::endl;
                } else {
                    dynamic_cast<Sword *>(this->getMWeapon())->setMDurability(durabilityLeft);
                }
            } else if (Game::getTypeOfWeapon(this->getMWeapon()) == "Dagger") {
                int currentDurability = dynamic_cast<Dagger *>(this->getMWeapon())->getMDurability();
                int durabilityLeft = currentDurability - 1;

                if (durabilityLeft <= 0) {
                    dynamic_cast<Dagger *>(this->getMWeapon())->setMDurability(0);
                    this->getMWeapon()->setMDamage(this->getMWeapon()->getMDamage() / 10);
                    std::cout << colors::blue << this->getMName() << colors::clear
                              << "'s weapon is broken ! His damages value is divided by 10 !"
                              << std::endl;
                } else {
                    dynamic_cast<Dagger *>(this->getMWeapon())->setMDurability(durabilityLeft);
                }

            } else if (Game::getTypeOfWeapon(this->getMWeapon()) == "Bow") {
                int currentArrows = dynamic_cast<Bow *>(this->getMWeapon())->getMArrows();
                int arrowsLeft = currentArrows - 1;

                if (arrowsLeft <= 0) {
                    dynamic_cast<Bow *>(this->getMWeapon())->setMArrows(0);
                    std::cout << colors::blue << this->getMName() << colors::clear << " has no more arrows !"
                              << std::endl;
                } else {
                    dynamic_cast<Bow *>(this->getMWeapon())->setMArrows(arrowsLeft);
                    std::cout << colors::blue << this->getMName() << colors::clear << " has " << arrowsLeft
                              << " arrows left" << std::endl;
                }

            } else if (Game::getTypeOfWeapon(this->getMWeapon()) == "Staff") {
                int currentMana = dynamic_cast<Sorcerer *>(this)->getMMana();
                int cost = dynamic_cast<Staff *>(this->getMWeapon())->getMCost();
                int manaLeft = currentMana - cost;

                if (manaLeft <= 0) {
                    dynamic_cast<Sorcerer *>(this)->setMMana(0);
                    std::cout << colors::blue << this->getMName() << colors::clear << " has no more mana !"
                              << std::endl;
                } else {
                    std::cout << colors::blue << this->getMName() << colors::clear << " has " << manaLeft
                              << " mana left" << std::endl;
                    dynamic_cast<Sorcerer *>(this)->setMMana(manaLeft);
                }
            }

        } else {
            std::cout << colors::blue << this->getMName() << colors::clear << " dodges the attack !" << std::endl;
        }
    } else {
        std::cerr << "This souldn't happen, the target has no owner" << std::endl;
    }
}

/// Allows a character to punch a target with his hands
/// \param p_target The target of the punch
void Character::punch(Character *p_target) {
    //If the target has an owner (just a simple check)
    if (p_target->getMOwner() != nullptr) {
        //The target is an invisible rogue
        //Check if the target is a rogue
        if (Game::getTypeOfCharacter(p_target) == "Rogue") {
            if (dynamic_cast<Rogue *>(p_target)->isMIsInvisible()) {
                if (Game::areAllPlayersVanished(p_target->getMOwner()->getMCharacters())) {
                    std::cout << "Oh too bad " << colors::blue << p_target->getMName() << colors::clear
                              << ", everyone has already vanished, you'll be targeted anyway ¯\\_(ツ)_/¯" << std::endl;
                } else {
                    throw VanishException(p_target->m_name + " has vanished");
                }
            }
        }
        //Warrior taunt check
        std::vector<Character *> charactersOfTheEnnemyPlayer = p_target->getMOwner()->getMCharacters();
        if (Game::isAnyCharacterShouting(charactersOfTheEnnemyPlayer) != nullptr) {
            p_target = Game::isAnyCharacterShouting(charactersOfTheEnnemyPlayer);
            std::cout << colors::blue << p_target->getMName() << colors::clear << " is shouting, " << this->getMName()
                      << " is forced to attack him !"
                      << std::endl;
        }
        //Dodge check
        float randomDodge = Game::getRandom(0.f, 1.f);
        float dodgeProb = p_target->getMDodge();
        //The warrior with a sword has an extra chance to dodge
        if (Game::getTypeOfCharacter(p_target) == "Warrior") {
            dodgeProb += dynamic_cast<Warrior *>(p_target)->getMBlock();
        }

        //The attack is a success
        if (dodgeProb < randomDodge) {
            int hpCurrent = p_target->getMLife();
            float coeff = Game::getRandom(0.95f, 1.05f);
            int damages = 16 * (this->getMAttack() / p_target->getMDefense()) * coeff;
            std::cout << colors::blue << this->getMName() << colors::clear << " inflicts " << colors::red << damages
                      << colors::clear << " damage points to " << colors::blue << p_target->getMName() << colors::clear
                      << " !" << std::endl;
            int hpLeft = hpCurrent - damages;
            if (hpLeft <= 0) {
                std::cout << colors::blue << p_target->getMName() << colors::clear << " is dead !" << std::endl;
                p_target->setMLife(0);
                p_target->setMName(colors::red + p_target->getMName() + " - DEAD X_X" + colors::clear);
            } else {
                p_target->setMLife(hpLeft);
            }

        } else {
            std::cout << colors::blue << p_target->getMName() << colors::clear << " dodges the attack !" << std::endl;
        }
    } else {
        std::cerr << "This souldn't happen, the target has no owner" << std::endl;
    }
}

Character::~Character() {
    delete this->m_weapon;
}

const std::string &Character::getMName() const {
    return m_name;
}

void Character::setMName(const std::string &mName) {
    m_name = mName;
}

int Character::getMLife() const {
    return m_life;
}

void Character::setMLife(int mLife) {
    m_life = mLife;
}

float Character::getMDodge() const {
    return m_dodge;
}

int Character::getMSpeed() const {
    return m_speed;
}

int Character::getMAttack() const {
    return m_attack;
}

int Character::getMDefense() const {
    return m_defense;
}

int Character::getMAgility() const {
    return m_agility;
}

int Character::getMIntelligence() const {
    return m_intelligence;
}

int Character::getMIsPoisoned() const {
    return m_isPoisoned;
}

void Character::setMIsPoisoned(int mIsPoisoned) {
    m_isPoisoned = mIsPoisoned;
}

Player *Character::getMOwner() const {
    return m_owner;
}

void Character::setMOwner(Player *mOwner) {
    m_owner = mOwner;
}

Weapon *Character::getMWeapon() const {
    return m_weapon;
}

/// Method used to calculate the new bonuses on the weapon switch
/// \param mWeapon the new weapon
void Character::setMWeapon(Weapon *mWeapon) {
    //The character already has a weapon and is desequipping it
    if (mWeapon == nullptr && this->m_weapon != nullptr) {
        this->m_attack -= this->m_weapon->getMBonus().at("attack");
        this->m_intelligence -= this->m_weapon->getMBonus().at("intelligence");
        this->m_agility -= this->m_weapon->getMBonus().at("agilite");
        this->m_life -= this->m_weapon->getMBonus().at("hp");
        //The character already has a weapon and is equipping another
    } else if (mWeapon != nullptr && this->m_weapon != nullptr) {
        //Remove the last bonuses
        this->m_attack -= this->m_weapon->getMBonus().at("attack");
        this->m_intelligence -= this->m_weapon->getMBonus().at("intelligence");
        this->m_agility -= this->m_weapon->getMBonus().at("agilite");
        this->m_life -= this->m_weapon->getMBonus().at("hp");
        // Add the newer ones
        this->m_attack += mWeapon->getMBonus().at("attack");
        this->m_intelligence += mWeapon->getMBonus().at("intelligence");
        this->m_agility += mWeapon->getMBonus().at("agilite");
        this->m_life += mWeapon->getMBonus().at("hp");
        //The character don't have a weapon and wants to equip one
    } else if (mWeapon != nullptr && this->m_weapon == nullptr) {
        this->m_attack += mWeapon->getMBonus().at("attack");
        this->m_intelligence += mWeapon->getMBonus().at("intelligence");
        this->m_agility += mWeapon->getMBonus().at("agilite");
        this->m_life += mWeapon->getMBonus().at("hp");
        //The character don't have any weapon and wants to desequip it
    } else {
        std::cerr << this->m_name << " don't have any weapon equipped, cannot desequip it" << std::endl;
    }
    m_weapon = mWeapon;
}

Character::Character(const std::string &mName, int mLife, float mDodge, int mSpeed, int mAttack, int mDefense,
                     int mAgility, int mIntelligence) : m_name(mName), m_life(mLife), BASE_LIFE(mLife),
                                                        m_dodge(mDodge),
                                                        m_speed(mSpeed),
                                                        m_attack(mAttack),
                                                        m_defense(mDefense),
                                                        m_agility(mAgility),
                                                        m_intelligence(mIntelligence),
                                                        m_isPoisoned(0) {
    this->m_owner = nullptr;
    this->m_weapon = nullptr;
}

void Character::decrementAllCooldowns() {

}

std::vector<std::string> Character::getAbilities() {
    return std::vector<std::string>();
}

int Character::getBaseLife() const {
    return BASE_LIFE;
}
