#ifndef MELEE_H
#define MELEE_H

#include "Weapon.h"

/**
 * Class representing a melee weapon
 */
class Melee : public Weapon {
protected:
    /**
     * The durability of the melee weapon
     */
    int m_durability;
    /**
     * If the melee is enchanted or not
     */
    bool m_isEnchanted;
    /**
     * The base durability of the weapon
     */
    int BASE_DURABILITY;

public:
    /**
     * Getter of durability
     * @return the durability of the melee
     */
    int getMDurability() const;

    /**
     * Setter of durability
     * @param mDurability the new durability
     */
    void setMDurability(int mDurability);

    /**
     * Getter of the enchanted value
     * @return if the weapon is enchanted or not
     */
    bool isMIsEnchanted() const;

    /**
     * Setter for the enchanted value
     * @param mIsEnchanted the value for the enchant
     */
    void setMIsEnchanted(bool mIsEnchanted);

    /**
     * Getter for the base durability
     * @return the base durability of the melee weapon
     */
    int getBaseDurability() const;

    /**
     * Constructor for a melee weapon
     * @param mName the name of the melee
     * @param mDamage the damage of the weapon
     * @param mCritical the critical value of the weapon
     * @param mDurability the durability of the weapon
     * @param mIsEnchanted if the weapon is enchanted or not
     */
    Melee(const std::string &mName, float mDamage, float mCritical, int mDurability, bool mIsEnchanted);

    /**
     * Destructor of melee
     */
    virtual ~Melee();
};

#endif
