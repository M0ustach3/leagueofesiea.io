#ifndef SWORD_H
#define SWORD_H

#include "Melee.h"

/**
 * Class representing a sword
 */
class Sword : public Melee {
public:
    /**
     * Constructor of a sword
     * @param mName the name
     * @param mDamage the damage
     * @param mCritical the critical probability
     * @param mDurability the durability
     * @param mIsEnchanted the enchantment value
     */
    Sword(const std::string &mName, float mDamage, float mCritical, int mDurability, bool mIsEnchanted);

    /**
     * Destructor of sword
     */
    virtual ~Sword();
};

#endif
