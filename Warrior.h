#ifndef WARRIOR_H
#define WARRIOR_H

#include <string>
#include <vector>

#include "Character.h"
#include "Weapon.h"
#include "Melee.h"

/**
 * Class used to describe a warrior
 */
class Warrior : public Character {
private:
    /**
     * The block value
     */
    float m_block;
    /**
     * The number of shouts remaining (represented by the rounds)
     */
    unsigned short m_shoutsRemaining;
    /**
     * The cooldown for a shout
     */
    unsigned short m_cooldownShout;

public:
    /**
     * Constructor of a warrior
     * @param mName the name
     * @param mLife the life
     * @param mDodge the dodge probability
     * @param mSpeed the speed
     * @param mAttack the attack
     * @param mDefense the defense
     * @param mAgility the agility
     * @param mIntelligence the intelligence
     * @param mBlock the block value
     */
    Warrior(const std::string &mName, int mLife, float mDodge, int mSpeed, int mAttack, int mDefense, int mAgility,
            int mIntelligence, float mBlock);

    /**
     * Method used to shout
     */
    void shout();

    /**
     * Method used to repair a weapon
     * @param p_weaponToRepair the weapon to repair
     */
    void repair(Weapon *p_weaponToRepair);

    /**
     * Destructor
     */
    virtual ~Warrior();

    /**
     * Getter for the block value
     * @return the block value
     */
    float getMBlock() const;

    /**
     * Getter for the remaining shouts
     * @return the number of remaining shouts
     */
    unsigned short getMShoutsRemaining() const;

    /**
     * Setter for the remaining shouts
     * @param mShoutsRemaining the new value for the remaining shouts
     */
    void setMShoutsRemaining(unsigned short mShoutsRemaining);

    /**
     * Getter for the cooldown of the shout
     * @return
     */
    unsigned short getMCooldownShout() const;

    /**
     * Getter for the weapon
     * @return the weapon of the warrior
     */
    Weapon *getMWeapon() const override;

    /**
     * Setter for the weapon of the warrior
     * @param mWeapon the new weapon
     */
    void setMWeapon(Weapon *mWeapon) override;

    /**
     * Method used to decrement all cooldowns
     */
    void decrementAllCooldowns() override;

    /**
     * Method used to get the abilities of the warrior
     * @return a vector of string representing the abilities
     */
    std::vector<std::string> getAbilities() override;
};

#endif
