#include <random>
#include "Game.h"
#include "Archer.h"
#include "Sorcerer.h"
#include "Warrior.h"
#include "Rogue.h"

Game::Game(Player *mPlayer1, Player *mPlayer2) : m_player1(mPlayer1), m_player2(mPlayer2) {
    this->m_currentPlayer = nullptr;
    m_speedCharacterJ1 = 0;
    m_speedCharacterJ2 = 0;
}

Player *Game::getMPlayer1() const {
    return m_player1;
}

void Game::setMPlayer1(Player *mPlayer1) {
    m_player1 = mPlayer1;
}

Player *Game::getMPlayer2() const {
    return m_player2;
}

void Game::setMPlayer2(Player *mPlayer2) {
    m_player2 = mPlayer2;
}

Game::~Game() {
    delete this->m_player1;
    delete this->m_player2;
}

/// Generates a random float between two numbers
/// \param p_min minimum generated value
/// \param p_max maximum generated value
/// \return a float generated between min value and max value
float Game::getRandom(float p_min, float p_max) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dis(p_min, p_max);
    return dis(mt);
}

/// Returns the class of a Character c
/// \param c A pointer to the character which needs to be identified
/// \return A string that contains the name of the class or "Unknown" if the class is unknown
std::string Game::getTypeOfCharacter(Character *c) {
    if (dynamic_cast<Archer *>(c)) {
        return "Archer";
    } else if (dynamic_cast<Sorcerer *>(c)) {
        return "Sorcerer";
    } else if (dynamic_cast<Warrior *>(c)) {
        return "Warrior";
    } else if (dynamic_cast<Rogue *>(c)) {
        return "Rogue";
    } else {
        return "Unknown";
    }
}

/// Method used to test if any character is shouting in a deck
/// \param targetDeck The deck to test
/// \return nullptr if no character is shouting, a pointer to the first shouting character in the deck
Character *Game::isAnyCharacterShouting(const std::vector<Character *> &targetDeck) {
    Character *p_target = nullptr;
    for (int i = 0; i < targetDeck.size(); ++i) {
        if (Game::getTypeOfCharacter(targetDeck[i]) == "Warrior") {
            if (dynamic_cast<Warrior *>(targetDeck[i]) != nullptr) {
                if (dynamic_cast<Warrior *>(targetDeck[i])->getMShoutsRemaining() > 0) {
                    p_target = (targetDeck[i]);
                }
            }
        }
    }
    return p_target;
}

/// Method used to get the type of a weapon
/// \param w The weapon to test
/// \return A string that contains type of the weapon (Sword, Dagger, Bow or Staff)
std::string Game::getTypeOfWeapon(Weapon *w) {
    if (dynamic_cast<Dagger *>(w)) {
        return "Dagger";
    } else if (dynamic_cast<Staff *>(w)) {
        return "Staff";
    } else if (dynamic_cast<Sword *>(w)) {
        return "Sword";
    } else if (dynamic_cast<Bow *>(w)) {
        return "Bow";
    } else {
        return "Unknown";
    }
}

/// Method used to see if all players in a team are invisible
/// \param targetDeck
/// \return true if all the players vanished, false otherwise
bool Game::areAllPlayersVanished(const std::vector<Character *> &targetDeck) {
    unsigned short coutVanished = 0;
    for (unsigned int i = 0; i < targetDeck.size(); i++) {
        Rogue *currentRogue;
        if ((currentRogue = dynamic_cast<Rogue *>(targetDeck.at(i))) && (currentRogue->isMIsInvisible())) {
            coutVanished++;
        }
    }
    return coutVanished == targetDeck.size();
}

void Game::nextRound() {
    if (this->m_currentPlayer != nullptr) {
        if (this->m_speedCharacterJ1 <= 0) {
            m_speedCharacterJ1 = 0;
            for (auto i : this->m_player1->getMCharacters()) {
                m_speedCharacterJ1 += i->getMSpeed();
            }
        }
        if (this->m_speedCharacterJ2 <= 0) {
            m_speedCharacterJ2 = 0;
            for (auto i : this->m_player2->getMCharacters()) {
                m_speedCharacterJ2 += i->getMSpeed();
            }
        }
        if (this->m_speedCharacterJ1 < m_speedCharacterJ2) {
            this->m_speedCharacterJ2 -= this->m_speedCharacterJ1;
            this->m_speedCharacterJ1 = 0;
            this->m_currentPlayer = this->m_player1;
        } else if (this->m_speedCharacterJ1 > m_speedCharacterJ2) {
            this->m_speedCharacterJ1 -= this->m_speedCharacterJ2;
            this->m_speedCharacterJ2 = 0;
            this->m_currentPlayer = this->m_player2;
        } else {
            float random = Game::getRandom(0, 1);
            if (random <= 0.5f) {
                this->m_speedCharacterJ2 -= this->m_speedCharacterJ1;
                this->m_speedCharacterJ1 = 0;
                this->m_currentPlayer = this->m_player1;
            } else {
                this->m_speedCharacterJ1 -= this->m_speedCharacterJ2;
                this->m_speedCharacterJ2 = 0;
                this->m_currentPlayer = this->m_player2;
            }
        }
        //Cooldowns and effects (shout)
        for (auto j : this->m_currentPlayer->getMCharacters()) {
            j->decrementAllCooldowns();
        }
        //Added poison
        for (auto k : this->m_currentPlayer->getMCharacters()) {
            if (k->getMIsPoisoned() > 0) {
                k->setMIsPoisoned(k->getMIsPoisoned() - 1);
            }
        }
    } else {
        float random = Game::getRandom(0, 1);
        if (random <= 0.5f) {
            this->m_speedCharacterJ2 -= this->m_speedCharacterJ1;
            this->m_speedCharacterJ1 = 0;
            this->m_currentPlayer = this->m_player1;
        } else {
            this->m_speedCharacterJ1 -= this->m_speedCharacterJ2;
            this->m_speedCharacterJ2 = 0;
            this->m_currentPlayer = this->m_player2;
        }
        //Cooldowns and effects (shout)
        for (auto j : this->m_currentPlayer->getMCharacters()) {
            j->decrementAllCooldowns();
        }
        for (auto k : this->m_currentPlayer->getMCharacters()) {
            if (k->getMIsPoisoned() > 0) {
                k->setMIsPoisoned(k->getMIsPoisoned() - 1);
            }
        }
    }
}

Player *Game::getMCurrentPlayer() const {
    return m_currentPlayer;
}

int Game::getRandomInt(int p_min, int p_max) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dis(p_min, p_max);
    return dis(mt);
}

Player *Game::getEnnemyOfCurrentPlayer() {
    if (this->m_currentPlayer == this->m_player1) {
        return this->m_player2;
    } else {
        return this->m_player1;
    }
}

bool Game::isCharacterCompatibleWithWeapon(Character *p_character, Weapon *p_weapon) {
    std::string typeCharacter = Game::getTypeOfCharacter(p_character);
    std::string typeWeapon = Game::getTypeOfWeapon(p_weapon);

    if (typeCharacter == "Archer") {
        return (typeWeapon == "Dagger" || typeWeapon == "Bow");
    } else if (typeCharacter == "Rogue") {
        return (typeWeapon == "Dagger" || typeWeapon == "Bow");
    } else if (typeCharacter == "Sorcerer") {
        return (typeWeapon == "Staff" || typeWeapon == "Sword");
    } else if (typeCharacter == "Warrior") {
        return (typeWeapon == "Sword" || typeWeapon == "Dagger");
    } else {
        return false;
    }
}

void Game::setFirstPlayer() {
    if (this->m_speedCharacterJ1 <= 0) {
        m_speedCharacterJ1 = 0;
        for (auto i : this->m_player1->getMCharacters()) {
            m_speedCharacterJ1 += i->getMSpeed();
        }
    }
    if (this->m_speedCharacterJ2 <= 0) {
        m_speedCharacterJ2 = 0;
        for (auto i : this->m_player2->getMCharacters()) {
            m_speedCharacterJ2 += i->getMSpeed();
        }
    }
    if (this->m_speedCharacterJ1 < m_speedCharacterJ2) {
        this->m_speedCharacterJ2 -= this->m_speedCharacterJ1;
        this->m_speedCharacterJ1 = 0;
        this->m_currentPlayer = this->m_player1;
    } else if (this->m_speedCharacterJ1 > m_speedCharacterJ2) {
        this->m_speedCharacterJ1 -= this->m_speedCharacterJ2;
        this->m_speedCharacterJ2 = 0;
        this->m_currentPlayer = this->m_player2;
    } else {
        float random = Game::getRandom(0, 1);
        if (random <= 0.5f) {
            this->m_speedCharacterJ2 -= this->m_speedCharacterJ1;
            this->m_speedCharacterJ1 = 0;
            this->m_currentPlayer = this->m_player1;
        } else {
            this->m_speedCharacterJ1 -= this->m_speedCharacterJ2;
            this->m_speedCharacterJ2 = 0;
            this->m_currentPlayer = this->m_player2;
        }
    }
}

bool Game::isPlayerOut(Player *p_player) {
    unsigned int cpt = 0;
    for (auto i : p_player->getMCharacters()) {
        if (i->getMLife() <= 0) {
            cpt++;
        }
    }
    return cpt == p_player->getMCharacters().size();
}
