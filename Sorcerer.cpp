#include <iostream>
#include "Sorcerer.h"
#include "exceptions/GameException.hpp"


Sorcerer::~Sorcerer() {
//Don't destroy weapons here, do it in main
}

unsigned int Sorcerer::getMMana() const {
    return m_mana;
}

void Sorcerer::setMMana(unsigned int mMana) {
    m_mana = mMana;
}

/// Method used to regenerate the mana from 2 to 7
void Sorcerer::regenerate() {
    if (this->m_mana == this->getBaseMana()) {
        throw ManaException("Already full mana");
    } else {
        int randMana = Game::getRandomInt(2, 7);
        this->m_mana += randMana;
        if (this->m_mana > this->BASE_MANA) {
            this->m_mana = this->BASE_MANA;
            std::cout << this->getMName() << " is now full mana !" << std::endl;
        } else {
            std::cout << this->getMName() << " regens " << randMana << " mana points !" << std::endl;
        }
    }
}

/// Method used to enchant a weapon by exchanging 7 mana
/// \param p_target the weapon to enchant (it has to be a melee weapon)
void Sorcerer::enchant(Weapon *p_target) {
    if (this->m_cooldownEnchant > 0) {
        std::cerr << this->m_name << " should wait for " << this->m_cooldownEnchant
                  << " more rounds before enchanting another weapon..." << std::endl;
    } else {
        Melee *melee;
        melee = dynamic_cast<Melee *>(p_target);
        if (melee) {
            if (this->m_mana < 7) {
                std::cerr << this->getMName() << " doesn't have enough mana to enchant" << std::endl;
            } else {
                melee->setMIsEnchanted(true);
                this->m_mana -= 7;
                std::cout << this->getMName() << " enchanted " << p_target->getMName() << " for its next use"
                          << std::endl;
            }
        } else {
            std::cerr << p_target->getMName() << " could't be enchanted (this is NOT a melee weapon)" << std::endl;
        }
        this->m_cooldownEnchant = 3;
    }
}

/// Method used to heal a player by exchanging 5 mana
/// the sorcerer has to have a staff
/// \param p_target the player to heal
void Sorcerer::heal(Character *p_target) {
    if (p_target->getMLife() == p_target->getBaseLife() + (int) p_target->getMWeapon()->getMBonus().at("hp")) {
        throw HealException(p_target->getMName() + " is already full life");
    } else if (this->m_weapon == nullptr ||
               (this->m_weapon != nullptr && Game::getTypeOfWeapon(this->m_weapon) != "Staff")) {
        throw HealException(this->getMName() + " doesn't have a staff to heal");
    } else if (this->m_mana < 5) {
        throw HealException(this->getMName() + " doesn't have enough mana to heal");
    } else {
        int randLife = Game::getRandomInt(10, 20);
        if (p_target->getMLife() + randLife >= p_target->getBaseLife()) {
            p_target->setMLife(p_target->getBaseLife());
            std::cout << this->getMName() << " heals " << p_target->getMName() << ". " << p_target->getMName()
                      << " is now full health !"
                      << std::endl;
        } else {
            p_target->setMLife(p_target->getMLife() + randLife);
            std::cout << this->getMName() << " heals " << p_target->getMName() << " for " << randLife << " HP !"
                      << std::endl;
        }
    }
}

Sorcerer::Sorcerer(const std::string &mName, int mLife, float mDodge, int mSpeed, int mAttack, int mDefense,
                   int mAgility, int mIntelligence) : Character(mName, mLife, mDodge,
                                                                mSpeed, mAttack,
                                                                mDefense, mAgility,
                                                                mIntelligence) {
    this->m_mana = this->m_intelligence * 10;
    this->BASE_MANA = this->m_mana;
    this->m_cooldownEnchant = 0;
}

unsigned int Sorcerer::getMCooldownEnchant() const {
    return m_cooldownEnchant;
}

Weapon *Sorcerer::getMWeapon() const {
    return Character::getMWeapon();
}

void Sorcerer::setMWeapon(Weapon *mWeapon) {
    if (Game::getTypeOfWeapon(mWeapon) == "Staff" || Game::getTypeOfWeapon(mWeapon) == "Sword") {
        Character::setMWeapon(mWeapon);
    } else {
        std::cerr << "Incompatible type" << std::endl;
    }
}

void Sorcerer::decrementAllCooldowns() {

    if (this->m_cooldownEnchant < 0) {
        this->m_cooldownEnchant--;
    }
}

std::vector<std::string> Sorcerer::getAbilities() {
    std::vector<std::string> abilities;

    abilities.push_back("Regenerate mana");
    abilities.push_back("Heal - Cost : 5 mana");

    if (this->getMCooldownEnchant() > 0)
        abilities.push_back(
                "Enchant weapon (Not available, " + std::to_string(this->getMCooldownEnchant()) + " rounds left)");
    else
        abilities.push_back("Enchant weapon - Cost : 7 mana");

    return abilities;
}

unsigned int Sorcerer::getBaseMana() const {
    return BASE_MANA;
}
