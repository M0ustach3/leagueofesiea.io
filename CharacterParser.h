#ifndef LEAGUEOFESIEA_IO_CHARACTERPARSER_H
#define LEAGUEOFESIEA_IO_CHARACTERPARSER_H

#include <string>
#include <ostream>

/**
 * Dummy class to use with the parser
 */
class CharacterParser {
public:
    CharacterParser();

    void setMClasse(const std::string &mClasse);

    void setMNom(const std::string &mNom);

    void setMVitesse(const std::string &mVitesse);

    void setMAttaque(const std::string &mAttaque);

    void setMIntelligence(const std::string &mIntelligence);

    void setMHp(const std::string &mHp);

    void setMDefense(const std::string &mDefense);

    void setMDodge(const std::string &mDodge);

    void setMAgilite(const std::string &mAgilite);

    void setMSpecial(const std::string &mSpecial);

    friend std::ostream &operator<<(std::ostream &os, const CharacterParser &parser);

    const std::string &getMClasse() const;

    const std::string &getMNom() const;

    const std::string &getMVitesse() const;

    const std::string &getMAttaque() const;

    const std::string &getMIntelligence() const;

    const std::string &getMHp() const;

    const std::string &getMDefense() const;

    const std::string &getMDodge() const;

    const std::string &getMAgilite() const;

    const std::string &getMSpecial() const;

private:
    std::string m_classe, m_nom, m_vitesse, m_attaque, m_intelligence, m_hp, m_defense, m_dodge, m_agilite, m_special;
};


#endif //LEAGUEOFESIEA_IO_CHARACTERPARSER_H
