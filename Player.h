#ifndef LEAGUEOFESIEA_IO_PLAYER_H
#define LEAGUEOFESIEA_IO_PLAYER_H

#include <string>
#include <vector>

class Character; //To fix circular includes

/**
 * Class used to describe a real player
 */
class Player {
private:
    /**
     * The name of the character
     */
    std::string m_name;
    /**
     * The characters of the player
     */
    std::vector<Character *> m_characters;
    /**
     * The current character of the player
     */
    unsigned short m_currentCharacter;

public:
    /**
     * Constructor of a player
     * @param mName the name of the player
     */
    Player(const std::string &mName);

    /**
     * Getter for the name of the player
     * @return the name of the player
     */
    const std::string &getMName() const;

    /**
     * Getter for the characters of the player
     * @return the characters of the players
     */
    const std::vector<Character *> &getMCharacters() const;

    /**
     * Setter for the characters of the player
     * @param mCharacters the new characters
     */
    void setMCharacters(const std::vector<Character *> &mCharacters);

    /**
     * Destructor of a player
     */
    virtual ~Player();

};

#endif //LEAGUEOFESIEA_IO_PLAYER_H