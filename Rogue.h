#ifndef ROGUE_H
#define ROGUE_H

#include <string>
#include <vector>
#include <iostream>

#include "Character.h"
#include "Bow.h"
#include "Dagger.h"

/**
 * Class describing a rogue
 */
class Rogue : public Character {
private:
    /**
     * The probability of poison
     */
    float m_poison;
    /**
     * Tells if the rogue is invisible
     */
    bool m_isInvisible;
    /**
     * The cooldown for the vanish method
     */
    unsigned short m_cooldownVanish;

public:
    /**
     * Constructor
     * @param mName the name
     * @param mLife the life
     * @param mDodge the dodge probability
     * @param mSpeed the speed
     * @param mAttack the attack
     * @param mDefense the defense
     * @param mAgility the agility
     * @param mIntelligence the intelligence
     * @param mPoison the poison probability
     */
    Rogue(const std::string &mName, int mLife, float mDodge, int mSpeed, int mAttack, int mDefense, int mAgility,
          int mIntelligence, float mPoison);

    /**
     * Method used to vanish
     */
    void vanish();

    /**
     * Destructor of a rogue
     */
    virtual ~Rogue();

    /**
     * Getter for the probability of poison
     * @return the probability of poison
     */
    float getMPoison() const;

    /**
     * Getter for the invisibility
     * @return true if the rogue is invisible, false otherwise
     */
    bool isMIsInvisible() const;

    /**
     * Setter for the invisibility
     * @param mIsInvisible the new visibility
     */
    void setMIsInvisible(bool mIsInvisible);

    /**
     * Getter for the weapon
     * @return the weapon of the rogue
     */
    Weapon *getMWeapon() const override;

    /**
     * Setter for the weapon
     * @param mWeapon the weapon for the character
     */
    void setMWeapon(Weapon *mWeapon) override;

    /**
     * Getter for the vanish cooldown
     * @return the cooldown for the vanish ability
     */
    unsigned short getMCooldownVanish() const;

    /**
     * Method used to decrease the cooldown for the vanish
     */
    void decreaseVanishCooldown();

    /**
     * Method used to decrement all cooldowns
     */
    void decrementAllCooldowns() override;

    /**
     * Method used to get the abilities for the rogue
     * @return a vector of string
     */
    std::vector<std::string> getAbilities() override;

};

#endif
